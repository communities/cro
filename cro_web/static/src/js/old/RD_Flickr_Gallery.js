/**
 * @module       RD Flickr Gallery
 * @author       Rafael Shayvolodyan
 * @see          https://ua.linkedin.com/in/rafael-shayvolodyan-3a297b96
 * @version      1.0.0
 */
(function () {
  (function (f, l, k) {
    var h;
    h = function () {
      function d(a, c) {
        this.options = f.extend(!0, {}, this.Defaults, c);
        this.$element = f(a);
        this.initialize()
      }
      d.prototype.Defaults = {
        flickrbase: "http://api.flickr.com/services/feeds/",
        feedapi: "photos_public.gne",
        qstrings: {
          ids: "",
          tags: "",
          tagmode: "",
          lang: "en-us",
          format: "json",
          jsoncallback: "?"
        },
        dateFormat: "%b/%d/%Y",
        cleanDescription: !0,
        callback: !1
      };
      d.prototype.initialize = function () {
        this.fetchData(this.makeUrl())
      };
      d.prototype.makeUrl = function () {
        var a, c, b, e;
        e = this.options.flickrbase + this.options.feedapi + "?";
        a = !0;
        b = {
          ids: this.$element.attr("data-flickr-id") ? this.$element.attr("data-flickr-id") : this.options.qstrings.ids,
          tags: this.$element.attr("data-flickr-tags") ? this.$element.attr("data-flickr-tags") : this.options.qstrings.tags,
          tagmode: this.$element.attr("data-flickr-tagmode") ? this.$element.attr("data-flickr-tagmode") : this.options.qstrings.tagmode,
          lang: this.$element.attr("data-flickr-lang") ? this.$element.attr("data-flickr-lang") : this.options.qstrings.lang,
          format: "json",
          jsoncallback: "?"
        };
        for (c in b) b[c] && (a || (e += "&"), e += c + "=" + b[c], a = !1);
        return e
      };
      d.prototype.fetchData = function (a) {
        var c;
        c = this;
        return f.getJSON(a, function (a) {
          f.each(a.items, function (a, b) {
            a < c.$element.find('[data-type="flickr-item"]').length && (c.options.cleanDescription && c.cleanDescription(b), c.setImageSizes(b), b.author_name = c.getAuthor(b.author), b.dating = c.dating(b.date_taken, !1), b.datetime = c.dating(b.date_taken, !0), c.setHTML(b, a))
          });
          "function" === typeof c.options.callback && c.options.callback()
        })
      };
      d.prototype.cleanDescription = function (a) {
        var c, b;
        b = /<p>(.*?)<\/p>/g;
        c = a.description;
        b.test(c) && (a.description = c.match(b)[2], null != a.description && (a.description = a.description.replace("<p>", "").replace("</p>", "")));
        return a
      };
      d.prototype.setImageSizes = function (a) {
        var c, b, e, d;
        d = "_s _q _t _m _n _- _z _c _b _h _k _o ".split(" ");
        c = 0;
        for (b = d.length; c < b; c++) e = d[c], a["image" + e] = a.media.m.replace("_m", e);
        delete a.media;
        return a
      };
      d.prototype.getAuthor = function (a) {
        a = /\(([^]+)\)/.exec(a);
        return null != a[1] ? a[1] : !1
      };
      d.prototype.dating = function (a, c) {
        var b, e, d, g, f;
        b = new Date(Date.parse(a));
        e = "January February March April May June July August September October November December".split(" ");
        b = {
          "%d": b.getDate(),
          "%m": b.getMonth() + 1,
          "%b": e[b.getMonth()].substr(0, 3),
          "%B": e[b.getMonth()],
          "%y": String(b.getFullYear()).slice(-2),
          "%Y": b.getFullYear()
        };
        a = c ? "%Y-%m-%d" : this.$element.attr("data-flickr-date-format") ? this.$element.attr("data-flickr-date-format") : this.options.dateFormat;
        d = a.match(/%[dmbByY]/g);
        g = 0;
        for (f = d.length; g < f; g++) e = d[g], a = a.replace(e, b[e]);
        return a
      };
      d.prototype.setHTML = function (a, c) {
        var b, e;
        e = this;
        b = this.$element.find('[data-type="flickr-item"]');
        e.parseAttributes(b.eq(c), a);
        b.eq(c).find("*").each(function () {
          e.parseAttributes(f(this), a)
        })
      };
      d.prototype.parseAttributes = function (a, c) {
        var b, e, d, g, f, h;
        d = a.data();
        for (g in d)
          if (d.hasOwnProperty(g) && "string" === typeof d[g])
            for (e = d[g].split(/\s?,\s?/i), f = 0, h = e.length; f < h; f++) b = e[f], "text" === b.toLowerCase() ? a.html(c[g]) : a.attr(b, c[g])
      };
      return d
    }();
    f.fn.extend({
      RDFlickr: function (d) {
        return this.each(function () {
          var a;
          a = f(this);
          if (!a.data("RDFlickr")) return a.data("RDFlickr", new h(this, d))
        })
      }
    });
    return k.RDFlickr = h
  })(window.jQuery, document, window);
  "undefined" !== typeof module && null !== module ? module.exports = window.RDFlickr : "function" === typeof define && define.amd && define(["jquery"], function () {
    return window.RDFlickr
  })
}).call(this);
