/**
 * @module       RD Parallax
 * @author       Evgeniy Gusarov
 * @see          https://ua.linkedin.com/pub/evgeniy-gusarov/8a/a40/54a
 * @version      3.6.4
 */
(function () {
  (function (n, m, d) {
    var t, x, y, v, h, l, w, r, u;
    l = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    y = /Chrome/.test(navigator.userAgent);
    r = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) || /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
    v = l && /crios/i.test(navigator.userAgent);
    w = /iPhone|iPad|iPod/i.test(navigator.userAgent) && !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
    h = -1 !== navigator.appVersion.indexOf("MSIE") ||
      -1 < navigator.appVersion.indexOf("Trident/");
    u = /windows nt 6.2/.test(navigator.userAgent.toLowerCase()) || /windows nt 6.3/.test(navigator.userAgent.toLowerCase());
    x = null != m.body.classList;
    chromeVersion = y ? navigator.userAgent.replace(/^.*Chrome\/([\d\.]+).*$/i, "$1") : !1;
    isChromeNew = "55.0.2883.75" <= chromeVersion;
    (function () {
      var h, l, m, b, a;
      l = 0;
      a = ["ms", "moz", "webkit", "o"];
      h = 0;
      for (m = a.length; h < m; h++) b = a[h], d.requestAnimationFrame = d[b + "RequestAnimationFrame"], d.cancelAnimationFrame = d[b + "CancelAnimationFrame"] ||
        d[b + "CancelRequestAnimationFrame"];
      d.requestAnimationFrame || (d.requestAnimationFrame = function (a, c) {
        var e, b, f;
        e = (new Date).getTime();
        f = Math.max(0, 16 - (e - l));
        b = d.setTimeout(function () {
          a(e + f)
        }, f);
        l = e + f;
        return b
      });
      if (!d.cancelAnimationFrame) return d.cancelAnimationFrame = function (a) {
        return clearTimeout(a)
      }
    });
    t = function () {
      function q(b) {
        this.options = n.extend(!0, {}, this.Defaults, b);
        this.scenes = [];
        this.initialize();
        this.scrollY = d.scrollY || d.pageYOffset;
        this.lastScrollY = -1;
        this.lastDocHeight = 0;
        this.checkLayerHeight =
          this.inputFocus = !1
      }
      var A, t;
      A = function () {
        function b(a, e, c, p, b, f, g) {
          this.amend = r || h || l ? isChromeNew ? 0 : 60 : 0;
          this.element = a;
          this.aliases = e;
          this.type = a.getAttribute("data-type") || "html";
          "html" === this.type && (this.holder = this.createHolder());
          this.direction = "normal" === a.getAttribute("data-direction") || null == a.getAttribute("data-direction") ? 1 : -1;
          this.fade = "true" === a.getAttribute("data-fade");
          this.blur = "true" === a.getAttribute("data-blur");
          this.boundTo = m.querySelector(a.getAttribute("data-bound-to"));
          "media" ===
          this.type && (this.url = a.getAttribute("data-url"));
          this.responsive = this.getResponsiveOptions();
          this.element.style.position = !h && !l || l || u && h ? "absolute" : "fixed";
          switch (this.type) {
            case "media":
              null != this.url && (this.element.style["background-image"] = "url(" + this.url + ")");
              break;
            case "html":
              h && l && (this.element.style["z-index"] = 1)
          }
          this.refresh(c, p, b, f, g)
        }
        b.prototype.refresh = function (a, e, c, p, b) {
          this.speed = this.getOption("speed", a) || 0;
          this.offset = this.getOption("offset", a) || 0;
          l || u && h || (this.element.style.position =
            b ? "fixed" : "absolute");
          h && "html" === this.type && (this.element.style.position = "absolute");
          switch (this.type) {
            case "media":
              if (!h) return this.offsetHeight = this.getMediaHeight(e, p, this.speed, this.direction), this.element.style.height = this.offsetHeight + "px";
              break;
            case "html":
              this.element.style.width = this.holder.offsetWidth + "px";
              this.offsetHeight = this.element.offsetHeight;
              this.holder.style.height = this.offsetHeight + "px";
              if (!(!h && !l || l || u && h)) return h ? this.element.style.position = "static" : b && (this.element.style.left =
                this.getOffset(this.holder).left + "px", this.element.style.top = this.getOffset(this.holder).top - c + "px"), this.holder.style.position = "static";
              break;
            case "custom":
              return this.offsetHeight = this.element.offsetHeight
          }
        };
        b.prototype.createHolder = function () {
          var a;
          a = m.createElement("div");
          x ? a.classList.add("rd-parallax-layer-holder") : a.className = "rd-parallax-layer-holder";
          this.element.parentNode.insertBefore(a, this.element);
          a.appendChild(this.element);
          if (!h && !l || v) a.style.position = "relative";
          return a
        };
        b.prototype.isHolderWrong =
          function () {
            return "html" === this.type && this.holder.offsetHeight !== this.element.offsetHeight ? !0 : !1
          };
        b.prototype.getOption = function (a, e) {
          var c, p;
          for (c in this.responsive) c <= e && (p = c);
          return this.responsive[p][a]
        };
        b.prototype.getResponsiveOptions = function () {
          var a, e, c, p, b, f, g, k, d;
          k = {};
          g = [];
          e = [];
          p = this.aliases;
          for (c in p) a = p[c], g.push(c), e.push(a);
          c = p = 0;
          for (b = g.length; p < b; c = ++p)
            for (f = g[c], k[f] = {}; - 1 <= (a = c);) !k[f].speed && (d = this.element.getAttribute("data" + e[a] + "speed")) && (k[f].speed = this.getSpeed(d)), !k[f].offset &&
              (d = this.element.getAttribute("data" + e[a] + "offset")) && (k[f].offset = parseInt(d)), !k[f].fade && (d = this.element.getAttribute("data" + e[a] + "fade")) && (k[f].fade = "true" === d), c--;
          return k
        };
        b.prototype.fuse = function (a, e) {
          var c, b, d;
          c = this.getOffset(this.element).top + this.element.getBoundingClientRect().top;
          b = a + e / 2;
          c += this.offsetHeight / 2;
          d = e / 6;
          b + d > c && b - d < c ? this.element.style.opacity = 1 : (b = b - d < c ? 1 + (b + d - c) / e / 3 * 10 : 1 - (b - d - c) / e / 3 * 10, this.element.style.opacity = 0 > b ? 0 : 1 < b ? 1 : b.toFixed(2))
        };
        b.prototype.move = function (a, e,
          c, b, d, f, g, k, m) {
          h && "media" === this.type || l || u && h || (g ? (g = !l || "html" === this.type && m || v ? this.speed * this.direction : this.speed * this.direction - 1, e = this.offsetHeight, null != k ? f = (b + c - (k + c)) / (c - d) : "media" !== this.type ? b < c || b > f - c ? (f = b < c ? b / (c - d) : (b + c - f) / (c - d), isFinite(f) || (f = 0)) : f = .5 : f = .5, a = v || h ? (d - e) / 2 + (c - d) * f * g + this.offset : l ? -(b - a) * g + (d - e) / 2 + (c - d) * f * (g + 1) + this.offset : -(b - a) * g + (d - e) / 2 + (c - d) * f * g + this.offset, l && null != k && (this.element.style.top = b - k + "px"), r && (this.element.style["-webkit-transform"] = "translate3d(0," +
            a + "px,0)"), this.element.style.transform = "translate3d(0," + a + "px,0)") : (r && (this.element.style["-webkit-transform"] = "translate3d(0,0,0)"), this.element.style.transform = "translate3d(0,0,0)"))
        };
        b.prototype.getSpeed = function (a) {
          return Math.min(Math.max(parseFloat(a), 0), 2)
        };
        b.prototype.getMediaHeight = function (a, b, c, d) {
          return b + (-1 === d ? (b + a) * c : 0) + (1 >= c ? Math.abs(a - b) * c : a * c) + 2 * this.amend
        };
        b.prototype.getOffset = function (a) {
          a = a.getBoundingClientRect();
          return {
            top: a.top + (d.scrollY || d.pageYOffset),
            left: a.left + (d.scrollX ||
              d.pageXOffset)
          }
        };
        return b
      }();
      t = function () {
        function b(a, b, c, d) {
          this.amend = r ? isChromeNew ? 0 : 60 : 0;
          this.element = a;
          this.aliases = b;
          this.on = !0;
          this.agent = m.querySelector(a.getAttribute("data-agent"));
          this.anchor = this.findAnchor();
          this.canvas = this.createCanvas();
          this.layers = this.createLayers(c);
          this.fitTo = this.getFitElement();
          this.responsive = this.getResponsiveOptions();
          this.refresh(c, d)
        }
        b.prototype.getFitElement = function () {
          var a;
          return null != (a = this.element.getAttribute("data-fit-to")) ? "parent" === a ? this.element.parentNode :
            m.querySelector(a) : null
        };
        b.prototype.findAnchor = function () {
          var a;
          for (a = this.element.parentNode; null != a && a !== m;) {
            if (this.isTransformed.call(a)) return a;
            a = a.parentNode
          }
          return null
        };
        b.prototype.createCanvas = function () {
          var a;
          a = m.createElement("div");
          x ? a.classList.add("rd-parallax-inner") : a.className = "rd-parallax-inner";
          for (this.element.appendChild(a); this.element.firstChild !== a;) a.appendChild(this.element.firstChild);
          this.element.style.position = "relative";
          this.element.style.overflow = "hidden";
          h || l ? (a.style.position =
            "absolute", u && h || (a.style.clip = "rect(0, auto, auto, 0)"), a.style.transform = h ? "translate3d(0,0,0)" : "none") : a.style.position = "fixed";
          a.style.left = this.offsetLeft + "px";
          a.style.top = 0;
          r && (a.style["margin-top"] = "-" + this.amend + "px", a.style.padding = this.amend + "px 0", this.element.style["z-index"] = 0);
          return a
        };
        b.prototype.getOption = function (a, b) {
          var c, e;
          for (c in this.responsive) c <= b && (e = c);
          return this.responsive[e][a]
        };
        b.prototype.getResponsiveOptions = function () {
          var a, b, c, d, h, f, g, k, l;
          k = {};
          g = [];
          b = [];
          d = this.aliases;
          for (c in d) a = d[c], g.push(c), b.push(a);
          c = d = 0;
          for (h = g.length; d < h; c = ++d)
            for (f = g[c], k[f] = {}; - 1 <= (a = c);) k[f].on || null == (l = this.element.getAttribute("data" + b[a] + "on")) || (k[f].on = "false" !== l), null == k[f].on && 0 === a && (k[f].on = !0), c--;
          return k
        };
        b.prototype.createLayers = function (a, b) {
          var c, e, d, f, g;
          e = n(this.element).find(".rd-parallax-layer").get();
          f = [];
          c = d = 0;
          for (g = e.length; d < g; c = ++d) c = e[c], f.push(new A(c, this.aliases, a, b, this.offsetTop, this.offsetHeight, this.on));
          return f
        };
        b.prototype.move = function (a) {
          a = null !=
            this.anchor ? this.positionTop : this.offsetTop - a;
          r && (this.canvas.style["-webkit-transform"] = "translate3d(0," + a + "px,0)");
          return this.canvas.style.transform = "translate3d(0," + a + "px,0)"
        };
        b.prototype.refresh = function (a, b) {
          var c, e, d, f, g;
          f = [];
          this.on = this.getOption("on", a);
          this.offsetTop = this.getOffset(this.element).top;
          this.offsetLeft = this.getOffset(this.element).left;
          this.width = this.element.offsetWidth;
          this.canvas.style.width = this.width + "px";
          null != this.anchor && (this.positionTop = this.element.offsetTop);
          null !=
            this.agent ? (this.agentOffset = this.getOffset(this.agent).top, this.agentHeight = this.agent.offsetHeight) : this.agentOffset = this.agentHeight = null;
          g = this.layers;
          c = 0;
          for (d = g.length; c < d; c++) e = g[c], "media" === e.type ? f.push(e) : e.refresh(a, b, this.offsetTop, this.offsetHeight, this.on);
          this.offsetHeight = this.canvas.offsetHeight - 2 * this.amend;
          this.element.style.height = this.offsetHeight + "px";
          c = 0;
          for (d = f.length; c < d; c++) e = f[c], e.refresh(a, b, this.offsetTop, this.offsetHeight, this.on)
        };
        b.prototype.update = function (a, b, c,
          d, m) {
          var e, g, k, p, n, q, z;
          z = this.offsetTop;
          q = this.offsetHeight;
          h || l || this.move(a);
          p = this.layers;
          n = [];
          e = 0;
          for (k = p.length; e < k; e++) g = p[e], g.move(a, b, c, z, q, d, this.on, this.agentOffset, m), g.fade = g.getOption("fade", b) || !1, !g.fade || l || h ? n.push(void 0) : n.push(g.fuse(z, q));
          return n
        };
        b.prototype.isTransformed = function () {
          var a, b, c;
          c = {
            webkitTransform: "-webkit-transform",
            OTransform: "-o-transform",
            msTransform: "-ms-transform",
            MozTransform: "-moz-transform",
            transform: "transform"
          };
          for (a in c) c.hasOwnProperty(a) && null !=
            this.style[a] && (b = d.getComputedStyle(this).getPropertyValue(c[a]));
          return null != b && 0 < b.length && "none" !== b ? !0 : !1
        };
        b.prototype.getOffset = function (a) {
          a = a.getBoundingClientRect();
          return {
            top: a.top + (d.scrollY || d.pageYOffset),
            left: a.left + (d.scrollX || d.pageYOffset)
          }
        };
        return b
      }();
      q.prototype.Defaults = {
        selector: ".rd-parallax",
        screenAliases: {
          0: "-",
          480: "-xs-",
          768: "-sm-",
          992: "-md-",
          1200: "-lg-",
          1920: "-xl-",
          2560: "-xxl-"
        }
      };
      q.prototype.initialize = function () {
        var b, a, e, c, h, l, f;
        b = this;
        e = m.querySelectorAll(b.options.selector);
        f = d.innerWidth;
        l = d.innerHeight;
        a = c = 0;
        for (h = e.length; c < h; a = ++c) a = e[a], b.scenes.push(new t(a, b.options.screenAliases, f, l));
        n(d).on("resize", n.proxy(b.resize, b));
        if (w) n("input").on("focusin focus", function (a) {
          a.preventDefault();
          b.activeOffset = n(this).offset().top;
          return d.scrollTo(d.scrollX || d.pageXOffset, b.activeOffset - this.offsetHeight - 100)
        });
        n(d).trigger("resize");
        b.update();
        b.checkResize()
      };
      q.prototype.resize = function (b) {
        var a, e, c;
        if ((a = d.innerWidth) !== this.windowWidth || !l || b) {
          this.windowWidth = a;
          this.windowHeight = d.innerHeight;
          this.documentHeight = m.body.offsetHeight;
          e = this.scenes;
          b = 0;
          for (a = e.length; b < a; b++) c = e[b], c.refresh(this.windowWidth, this.windowHeight);
          return this.update(!0)
        }
      };
      q.prototype.update = function (b) {
        var a, e, c, h, n, f, g, k, q, r;
        e = this;
        b || requestAnimationFrame(function () {
          e.update()
        });
        k = d.scrollY || d.pageYOffset;
        w && null != (a = m.activeElement) && (a.tagName.match(/(input)|(select)|(textarea)/i) ? (e.activeElement = a, e.inputFocus = !0) : (e.activeElement = null, e.inputFocus = !1, b = !0));
        l && y && (a = d.innerHeight -
          e.windowHeight, e.deltaHeight = a, k -= e.deltaHeight);
        if ((k !== e.lastScrollY || b) && !e.isActing) {
          e.isActing = !0;
          r = e.windowWidth;
          q = e.windowHeight;
          c = e.documentHeight;
          a = k - e.lastScrollY;
          w && null != e.activeElement && (e.activeElement.value += " ", e.activeElement.value = e.activeElement.value.trim());
          f = e.scenes;
          h = 0;
          for (n = f.length; h < n; h++) g = f[h], (e.inputFocus || b || k + q >= (g.agentOffset || g.offsetTop) + a && k <= (g.agentOffset || g.offsetTop) + (g.agentHeight || g.offsetHeight) + a) && g.update(k, r, q, c, e.inputFocus);
          e.lastScrollY = k;
          return e.isActing = !1
        }
      };
      q.prototype.checkResize = function () {
        var b;
        b = this;
        setInterval(function () {
          var a, e, c, d, h, f, g, k;
          a = m.body.offsetHeight;
          g = b.scenes;
          e = 0;
          for (h = g.length; e < h; e++) {
            c = g[e];
            k = c.layers;
            c = 0;
            for (f = k.length; c < f; c++)
              if (d = k[c], d.isHolderWrong()) {
                b.checkLayerHeight = !0;
                break
              }
            if (b.checkLayerHeight) break
          }
          if (b.checkLayerHeight || a !== b.lastDocHeight) return b.resize(!0), b.lastDocHeight = a, b.checkLayerHeight = !1
        }, 500)
      };
      return q
    }();
    n.RDParallax = function (d) {
      var h;
      h = n(m);
      if (!h.data("RDParallax")) return h.data("RDParallax", new t(d))
    };
    return d.RDParallax = t
  })(window.jQuery, document, window);
  "undefined" !== typeof module && null !== module ? module.exports = window.RDParallax : "function" === typeof define && define.amd && define(["jquery"], function () {
    return window.RDParallax
  })
}).call(this);
