

{
    'name': 'Centro de Recolha Oficial de Animais - Portal Adopção',
    'summary': 'Support for Bootswatch themes in master',
    'description': 'This theme module is exclusively for CROA',
    'category': 'Theme',
    'sequence': 900,
    'version': '1.0',
    'depends': ['website'],
    'data': [
        'views/assets.xml',
        'views/template.xml',
        'views/homepage.xml',
        #'data/theme_bootswatch_data.xml',
        #'views/theme_bootswatch_templates.xml',
    ],
    'images': ['static/description/bootswatch.png'],
    'author': "Communities - Comunicações, Lda",
    'website': "http://www.croa.pt",
    'application': False,
}