# -*- coding: utf-8 -*-
from odoo import http, _
from odoo.exceptions import AccessError
from odoo.http import request

class WebsiteController(http.Controller):
	
    @http.route('/page/homepage', type="http", auth="public", website=True)
    def croa_clients(self, **kwargs):
        animals = http.request.env['cro.animal'].sudo().search([('state','=', 'available')])
        dogs = request.env['cro.animal'].sudo().search_count([('especie_id','=', 'Canina')])
        organizations = http.request.env['res.partner'].sudo().search([('in_directory','=', True)])

        
        return http.request.render('website.homepage', {'organizations': organizations, 'animals': animals, 'dogs': dogs} )
