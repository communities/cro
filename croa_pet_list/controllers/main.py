# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import datetime
import werkzeug

from collections import OrderedDict

from odoo import fields
from odoo import http
from odoo.http import request
from odoo.addons.website.models.website import slug, unslug
from odoo.addons.croa_pet_list.controllers.pet import WebsitePetPage
from odoo.tools.translate import _

from odoo.addons.website_portal.controllers.main import website_account



class WebsitePetList(WebsitePetPage):
    _references_per_page = 8

    @http.route([
        #'/pets',

        #'/pets/state/<model("res.country.state"):state>',
        '/pets',
        '/pets/page/<int:page>',

        '/pets/especie/<model("cro.animal.especie"):especie>',
        '/pets/especie/<model("cro.animal.especie"):especie>/page/<int:page>',

        '/pets/state/<model("res.country.state"):state>',
        '/pets/state/<model("res.country.state"):state>/page/<int:page>',

        '/pets/especie/<model("cro.animal.especie"):especie>/state/<model("res.country.state"):state>',
        '/pets/especie/<model("cro.animal.especie"):especie>/state/<model("res.country.state"):state>/page/<int:page>',
    ], type='http', auth="public", website=True)
    def pets(self, state=None, especie=None, page=0, **post):
        state_all = post.pop('state_all', False)
        pet_obj = request.env['cro.animal']
        state_obj = request.env['res.country.state']
        search = post.get('search', '')

        base_pet_domain = [('state', '=', 'resident'), ('especie_id', '!=', False), ('website_published', '=', True)]
        if not request.env['res.users'].has_group('website.group_website_publisher'):
            base_pet_domain += [('especie_id.website_published', '=', True)]
        if search:
            base_pet_domain += ['|', ('name', 'ilike', search), ('website_description', 'ilike', search)]

        # group by especie
        especie_domain = list(base_pet_domain)
        if not state and not state_all:
            state_code = request.session['geoip'].get('state_code')
            if state_code:
                state = state_obj.search([('code', '=', state_code)], limit=1)
        if state:
            especie_domain += [('state_id', '=', state.id)]
        especies = pet_obj.sudo().read_group(
            especie_domain, ["id", "especie_id"],
            groupby="especie_id", orderby="especie_id DESC")
        especies_pets = pet_obj.sudo().search_count(especie_domain)
        # flag active especie
        for especie_dict in especies:
            especie_dict['active'] = especie and especie_dict['especie_id'][0] == especie.id
        especies.insert(0, {
            'especie_id_count': especies_pets,
            'especie_id': (0, _("Todos os tipos")),
            'active': bool(especie is None),
        })


        # group by state
        state_domain = list(base_pet_domain)
        if especie:
            state_domain += [('especie_id', '=', especie.id)]
        states = pet_obj.sudo().read_group(
            state_domain, ["id", "state_id"],
            groupby="state_id", orderby="state_id")
        states_pets = pet_obj.sudo().search_count(state_domain)
        # flag active state
        for state_dict in states:
            state_dict['active'] = state and state_dict['state_id'] and state_dict['state_id'][0] == state.id
        states.insert(0, {
            'state_id_count': states_pets,
            'state_id': (0, _("Todos os Distritos")),
            'active': bool(state is None),
        })

        # current search
        if especie:
            base_pet_domain += [('especie_id', '=', especie.id)]
        if state:
            base_pet_domain += [('state_id', '=', state.id)]

        # format pager
        if especie and not state:
            url = '/pets/especie/' + slug(especie)
        elif state and not especie:
            url = '/pets/state/' + slug(state)
        elif state and especie:
            url = '/pets/especie/' + slug(especie) + '/state/' + slug(state)
        
        # format pager
        if especie and not state:
            url = '/pets/especie/' + slug(especie)
        elif state and not especie:
            url = '/pets/state/' + slug(state)
        elif state and especie:
            url = '/pets/especie/' + slug(especie) + '/state/' + slug(state)
        else:
            url = '/pets'
        url_args = {}
        if search:
            url_args['search'] = search
        if state_all:
            url_args['state_all'] = True

        pet_count = pet_obj.sudo().search_count(base_pet_domain)
        pager = request.website.pager(
            url=url, total=pet_count, page=page, step=self._references_per_page, scope=7,
            url_args=url_args)

        # search pets matching current search parameters
        pet_ids = pet_obj.sudo().search(
            base_pet_domain, order="especie_id DESC, display_name ASC")  # todo in trunk: order="especie_id DESC, implemented_count DESC", offset=pager['offset'], limit=self._references_per_page
        pets = pet_ids.sudo()
        # remove me in trunk
        #pets = sorted(pets, key=lambda x: (x.id if x.id else 1, len([i for i in x.pet_ids if i.website_published])), reverse=True)
        #pets = sorted(pets, key=lambda x: (x.especie_id.sequence if x.especie_id else 0, len([i for i in x.implemented_pet_ids if i.website_published])), reverse=True)
        pets = pets[pager['offset']:pager['offset'] + self._references_per_page]


        values = {
            'states': states,
            'current_state': state,
            'especies': especies,
            'current_especie': especie,
            'pets': pets,
            'pager': pager,
            'searches': post,
            'search_path': "%s" % werkzeug.url_encode(post),
        }
        return request.render("croa_pet_list.index", values, status=pets and 200 or 404)
