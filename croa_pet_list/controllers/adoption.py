# -*- coding: utf-8 -*-
import werkzeug
import string
import random
from datetime import datetime
import json
import math
import base64
import logging
_logger = logging.getLogger(__name__)

from odoo import http
from odoo.http import request

class WebsiteCroController(http.Controller):

    @http.route('/partner/register', type="http", auth="public", website=True)
    def croa_partner_register(self, **kwargs):
        countries = request.env['res.country'].search([])
        states = request.env['res.country.state'].search([])
        
        return http.request.render('croa_partner_list.inscription', {'countries': countries, 'states': states} )

###########################################################################################################

    @http.route('/partner/register/process', type="http", auth="public", website=True, csrf=False)
    def croa_partner_register_process(self, **kwargs):

        values = {}
	for field_name, field_value in kwargs.items():
	    values[field_name] = field_value

        if request.env['res.users'].sudo().search_count([('login','=',values['login'])]) > 0:
            #return werkzeug.utils.redirect("/page/user_erro")
            return http.request.render('croa_partner_list.user_error_page', {'user_mail': values['login']} )

        #Create the new user
        password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
        new_user = request.env['res.users'].sudo().create({'name': values['name'], 'phone': values['phone'], 'mobile': values['mobile'], 'cedula': values['cedula'], 'action_id': 77, 'login': values['login'], 'password': password})
        
        print ("############# USER PASSWORD##############")
        print password

        group_cro_public = request.env['ir.model.data'].sudo().get_object('cro_core', 'group_cro_public')
        group_cro_public.users = [(4, new_user.id)]    
            
	     #Add the user to the groups
        if values['partner_type'] == 'croa':
            group_cro_back_office = request.env['ir.model.data'].sudo().get_object('cro_core', 'group_cro_back_office')
            group_cro_back_office.users = [(4, new_user.id)]
            
        if values['partner_type'] == 'camv':
            group_cro_camv = request.env['ir.model.data'].sudo().get_object('cro_core', 'group_cro_camv')
            group_cro_camv.users = [(4, new_user.id)]
        
        #insert_values = {'user_ids': [new_user.id]} #Add user to the partner
        insert_values = {'user_ids': [(6, 0, [new_user.id])]} #Add user to the partner
        #insert_values = {'name': values['p_name']}
        
        #insert_values['name'] = values['p_name']
        if values['partner_type'] == 'camv': insert_values['name'] = values['cname']
        if values['partner_type'] == 'croa': insert_values['name'] = values['mname']
        if 'rml_header1' in values: insert_values['rml_header1'] = values['rml_header1'] 
        if 'street' in values: insert_values['street'] = values['street'] 
        if 'city' in values: insert_values['city'] = values['city'] 
        if 'dist' in values: insert_values['state_id'] = values['dist'] 
        if 'zip' in values: insert_values['zip'] = values['zip'] 
        if 'website' in values: insert_values['website'] = values['website'] 
        if 'p_vat' in values: insert_values['vat'] = values['p_vat'] 
        if 'email' in values: insert_values['email'] = values['email'] 
        if 'p_phone' in values: insert_values['phone'] = values['p_phone']
        if 'partner_type' in values: insert_values['partner_type'] = values['partner_type']
        if 'camv_type' in values: insert_values['camv_type'] = values['camv_type'] 
        if 'iban' in values: insert_values['iban'] = values['iban'] 
        
        #if 'parent_id' in values: insert_values['parent_id'] = values[''] #(sempre entidade croa e omv - se for CAMV escolher CROAs ???????)
        #if 'child_ids' in values: insert_values['child_ids'] = values[''] #(se croa escolher uma lista de camvs)
        
        if 'logo' in values: insert_values['logo'] = base64.encodestring(values['logo'].read())
        
        new_partner = request.env['res.company'].sudo().create(insert_values) 	   
       
        #Modify the users company
        #new_user.write({'company_ids': [new_partner.id], 'company_id': new_partner.id})
        new_user.write({'company_id': new_partner.id})
        
        print ('#####################################')
        print new_partner.id
        print new_partner.name
        print ('#####################################')
        
        #Modify the users partner record
        new_user.partner_id.write({'is_company': False, 'phone': values['phone'], 'mobile': values['mobile'], 'email': values['login'], 'parent_id': new_partner.partner_id.id})         
       
        return request.render("croa_partner_list.inscription_result")

