# -*- coding: utf-8 -*-

from odoo import http
from odoo.http import request
from odoo.addons.website.models.website import unslug


class WebsitePetPage(http.Controller):

    # Do not use semantic controller due to SUPERUSER_ID
    @http.route(['/pets/<pet_id>'], type='http', auth="public", website=True)
    def pets_detail(self, pet_id, **post):
        _, pet_id = unslug(pet_id)
        if pet_id:
            pet_sudo = request.env['cro.animal'].sudo().browse(pet_id)
            is_website_publisher = request.env['res.users'].has_group('website.group_website_publisher')
            if pet_sudo.exists() and (pet_sudo.website_published or is_website_publisher):
                values = {
                    'main_object': pet_sudo,
                    'pet': pet_sudo,
                    'edit_page': False
                }
                return request.render("croa_pet_list.pet_page", values)
        return request.not_found()
