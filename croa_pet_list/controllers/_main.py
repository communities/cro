# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import datetime
import werkzeug

from collections import OrderedDict

from odoo import fields
from odoo import http
from odoo.http import request
from odoo.addons.website.models.website import slug, unslug
from odoo.addons.croa_pet_list.controllers.pet import WebsitePetPage
from odoo.tools.translate import _

from odoo.addons.website_portal.controllers.main import website_account



class WebsitePetList(WebsitePetPage):
    _references_per_page = 8

    @http.route([

        '/pets',
        '/pets/page/<int:page>',

        '/pets/especie/<model("cro.animal.especie"):especie>',
        '/pets/especie/<model("cro.animal.especie"):especie>/page/<int:page>',

        '/pets/croa/<model("res.company"):croa>',
        '/pets/croa/<model("res.company"):croa>/page/<int:page>',

        '/pets/especie/<model("cro.animal.especie"):especie>/croa/<model("res.company"):croa>',
        '/pets/especie/<model("cro.animal.especie"):especie>/croa/<model("res.company"):croa>/page/<int:page>',
    ], type='http', auth="public", website=True)
    def pets(self, croa=None, especie=None, page=0, **post):
        croa_all = post.pop('croa_all', False)
        pet_obj = request.env['cro.animal']
        search = post.get('search', '')

        base_pet_domain = [('state', '=', 'resident'), ('especie_id', '!=', False), ('website_published', '=', True)]
        if not request.env['res.users'].has_group('website.group_website_publisher'):
            base_pet_domain += [('especie_id.website_published', '=', True)]
        if search:
            base_pet_domain += ['|', ('name', 'ilike', search), ('website_description', 'ilike', search)]

        # group by especie
        especie_domain = list(base_pet_domain)
        if croa:
            especie_domain += [('croa_id', '=', croa.id)]
        especies = pet_obj.sudo().read_group(
            especie_domain, ["id", "especie_id"],
            groupby="especie_id", orderby="especie_id DESC")
        especies_pets = pet_obj.sudo().search_count(especie_domain)
        # flag active especie
        for especie_dict in especies:
            especie_dict['active'] = especie and especie_dict['especie_id'][0] == especie.id
        especies.insert(0, {
            'especie_id_count': especies_pets,
            'especie_id': (0, _("Todos os tipos")),
            'active': bool(especie is None),
        })


        # group by croa
        croa_domain = list(base_pet_domain)
        if especie:
            croa_domain += [('especie_id', '=', especie.id)]
        croas = pet_obj.sudo().read_group(
            croa_domain, ["id", "croa_id"],
            groupby="croa_id", orderby="croa_id")
        croas_pets = pet_obj.sudo().search_count(croa_domain)
        # flag active croa
        for croa_dict in croas:
            #croa_dict['active'] = croa and croa_dict['croa_id'] and croa_dict['croa_id'][0] == croa.id
            croa_dict['active'] = croa and croa_dict['croa_id'][0] == croa.id
        croas.insert(0, {
            'croa_id_count': croas_pets,
            'croa_id': (0, _("Todos os Distritos")),
            'active': bool(croa is None),
        })

        # current search
        if especie:
            base_pet_domain += [('especie_id', '=', especie.id)]
        if croa:
            base_pet_domain += [('croa_id', '=', croa.id)]

        # format pager
        if especie and not croa:
            url = '/pets/especie/' + slug(especie)
        elif croa and not especie:
            url = '/pets/croa/' + slug(croa)
        elif croa and especie:
            url = '/pets/especie/' + slug(especie) + '/croa/' + slug(croa)
        
        # format pager
        if especie and not croa:
            url = '/pets/especie/' + slug(especie)
        elif croa and not especie:
            url = '/pets/croa/' + slug(croa)
        elif croa and especie:
            url = '/pets/especie/' + slug(especie) + '/croa/' + slug(croa)
        else:
            url = '/pets'
        url_args = {}
        if search:
            url_args['search'] = search
        if croa_all:
            url_args['croa_all'] = True

        pet_count = pet_obj.sudo().search_count(base_pet_domain)
        pager = request.website.pager(
            url=url, total=pet_count, page=page, step=self._references_per_page, scope=7,
            url_args=url_args)

        # search pets matching current search parameters
        pet_ids = pet_obj.sudo().search(
            base_pet_domain, order="especie_id DESC, display_name ASC")  # todo in trunk: order="especie_id DESC, implemented_count DESC", offset=pager['offset'], limit=self._references_per_page
        pets = pet_ids.sudo()
        # remove me in trunk
        #pets = sorted(pets, key=lambda x: (x.id if x.id else 1, len([i for i in x.pet_ids if i.website_published])), reverse=True)
        #pets = sorted(pets, key=lambda x: (x.especie_id.sequence if x.especie_id else 0, len([i for i in x.implemented_pet_ids if i.website_published])), reverse=True)
        pets = pets[pager['offset']:pager['offset'] + self._references_per_page]


        values = {
            'croas': croas,
            'current_croa': croa,
            'especies': especies,
            'current_especie': especie,
            'pets': pets,
            'pager': pager,
            'searches': post,
            'search_path': "%s" % werkzeug.url_encode(post),
        }
        return request.render("croa_pet_list.index", values, status=pets and 200 or 404)
