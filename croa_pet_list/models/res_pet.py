# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools, _
from odoo.addons.website.models.website import slug
from odoo.exceptions import UserError, ValidationError

class ResPetEspecie(models.Model):
    _inherit = "cro.animal.especie"

    website_published = fields.Boolean(default=False)
   
class ResPetWebsiteImage(models.Model):
    _name = 'cro.animal.image'
    _description = 'Additional Image'

    name = fields.Char(string='Nome')
    description = fields.Text(string='Descrição')
    image_alt = fields.Text(string='Texto alternativo')
    image = fields.Binary(string='Imagem')
    image_small = fields.Binary(string='Small Image')
    image_url = fields.Char(string='URL externo')
    image_id = fields.Many2one('cro.animal', 'Animal',
                                      copy=False)

class ResPetWebsite(models.Model):
    #_inherit = "cro.animal"
    _name = 'cro.animal'
    _inherit = ['cro.animal', 'website.published.mixin']

    #website_published = fields.Boolean(default=False)
    website_description = fields.Html('Informação adicional', strip_style=True)
    state_id = fields.Many2one("res.country.state", string='Distrito', ondelete='restrict')
    images = fields.One2many('cro.animal.image', 'image_id', 'Imagens Adicionais')

    @api.constrains('dono_id')
    @api.one
    def _onchange_dono_id(self):
        if not self.dono_id:
            return
        dono = self.dono_id
        self.state_id = dono.state_id

    @api.multi
    def _compute_website_url(self):
        super(ResPetWebsite, self)._compute_website_url()
        for pet in self:
            pet.website_url = "/pets/%s" % slug(pet)

    @api.multi
    def website_publish_button(self):
        self.ensure_one()
        if not self.state_id:
            raise ValidationError("Faltam dados necessários para publicação no Portal de Adopção!!!!\n"
                                    "Verifique os dados de morada do Centro de Recolha Oficial.\n")
        if self.env.user.has_group('website.group_website_publisher') and self.website_url != '#':
            return self.open_website_url()
        return self.write({'website_published': not self.website_published})     