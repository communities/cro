# -*- coding: utf-8 -*-
# Author: Damien Crier, Andrea Stirpe, Kevin Graveman, Dennis Sluijk
# Author: Julien Coux
# Copyright 2016 Camptocamp SA, Onestein B.V.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from datetime import datetime
from odoo import api, fields, models
from odoo.tools.safe_eval import safe_eval


class PartnerBalance(models.TransientModel):
    """partner balance report wizard."""

    _name = 'cro.partner.balance.wizard'
    _description = 'Partner Balance Wizard'

    company_id = fields.Many2one(
        comodel_name='res.company',
        default=lambda self: self.env.user.company_id,
        string='Company'
    )
    date_at = fields.Date(required=True,
                          default=fields.Date.to_string(datetime.today()))
    target_move = fields.Selection([('done', 'Só Cheques Utilizados'),
                                    ('all', 'Todos os Cheques')],
                                   string='Target Moves',
                                   required=True,
                                   default='all')
    tipo_cheque_ids = fields.Many2many(
        comodel_name='cro.tipo.cheque',
        string='Tipo de Cheque',
    )

    partner_ids = fields.Many2many(
        comodel_name='res.partner',
        string='Filtrar Parceiros',
    )
    show_move_line_details = fields.Boolean()



    @api.multi
    def button_export_html(self):
        self.ensure_one()
        action = self.env.ref(
            'cro_cheque_reports.action_report_partner_balance')
        vals = action.read()[0]
        context1 = vals.get('context', {})
        if isinstance(context1, basestring):
            context1 = safe_eval(context1)
        model = self.env['report_partner_balance_qweb']
        report = model.create(self._prepare_report_partner_balance())
        report.compute_data_for_report()
        context1['active_id'] = report.id
        context1['active_ids'] = report.ids
        vals['context'] = context1
        return vals

    @api.multi
    def button_export_pdf(self):
        self.ensure_one()
        report_type = 'qweb-pdf'
        return self._export(report_type)

    @api.multi
    def button_export_xlsx(self):
        self.ensure_one()
        report_type = 'xlsx'
        return self._export(report_type)

    def _prepare_report_partner_balance(self):
        self.ensure_one()
        return {
            'date_at': self.date_at,
            'only_posted_moves': self.target_move == 'done',
            'company_id': self.company_id.id,
            'filter_tipo_cheque_ids': [(6, 0, self.tipo_cheque_ids.ids)],
            'filter_partner_ids': [(6, 0, self.partner_ids.ids)],
            'show_move_line_details': self.show_move_line_details,
        }

    def _export(self, report_type):
        """Default export is PDF."""
        model = self.env['report_partner_balance_qweb']
        report = model.create(self._prepare_report_partner_balance())
        report.compute_data_for_report()
        return report.print_report(report_type)
