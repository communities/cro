# -*- coding: utf-8 -*-
{
    'name': "Cheque Veterinário Relatórios",

    'summary': """
        """,

    'description': """
        Centro de Recolha Oficial de Animais - Cheque Veterinário Relatórios
    """,

    'author': "Communities - Comunicações, Lda",
    'website': "http://www.cro.pt",

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'cro_core', 'cro_cheque', 'report_xlsx', 'report'],

    # always loaded
    'data': [
        'wizard/partner_balance_wizard_view.xml',
        'menuitems.xml',
        'reports.xml',
        'report/templates/partner_balance.xml',
        'view/report_partner_balance.xml',
    ],
}
