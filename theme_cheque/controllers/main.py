# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http
from odoo.http import request

class WebsiteCheque(http.Controller):
    _partner_per_page = 6
    
    @http.route('/page/camvs', type='http', auth="public", website=True)
    def camvs(self, **post):

        camvs = request.env['res.company'].sudo().search([('active','=',True), ('partner_type','=','camv')])
        return request.render("theme_cheque.camvs", {'camvs': camvs})

    @http.route(['/page/croas','/page/croas/page/<int:page>'], type='http', auth="public", website=True)
    def croas(self, page=1, **post):
           
            
        ActiveCroas = request.env['res.company'].sudo().search([('active','=',True), ('partner_type','=','croa')])
        total = ActiveCroas.search([], count=True)
            
        pager = request.website.pager(
            url='/page/croas',
            total=total,
            page=page,
            step=self._partner_per_page,
        )   
        croas = ActiveCroas.search([], offset=(page - 1) * self._partner_per_page, limit=self._partner_per_page)      
        
        return request.render("theme_cheque.croas", {'croas': croas, 'pager': pager})