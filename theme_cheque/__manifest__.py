{
    'name': 'Cheque Veterinário',
    'description': 'Cheque Theme',
    'category': 'Theme/Services',
    'sequence': 120,
    'version': '1.1',
    'author': 'AyA - Soluctions',
    'depends': ['theme_common', 'website_animate'],
    'data': [
        'views/assets.xml',
        'views/customize_modal.xml',
        'views/image_content.xml',
        'views/image_library.xml',
        'views/snippets_options.xml',
        'views/snippets.xml',
        'views/debranding.xml',
        'views/pages/cheque_template.xml',
        'views/pages/login_template.xml',
        'views/pages/croas_template.xml',
        'views/pages/camvs_template.xml',

    ],
    'images': [
        'static/description/Cheque_description.jpg',
    ],
}
