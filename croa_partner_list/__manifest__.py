# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'CROA Partners',
    'category': 'Website',
    'website': 'https://www.communities.pt',
    'summary': 'Publish Your Partners',
    'version': '1.0',
    'description': """
Publish and Assign Partner
        """,
    'depends': ['website_partner'],
    'data': [
        'security/ir.model.access.csv',
        'views/website_partner_templates.xml',
        'views/website_inscription_templates.xml',
        'views/res_partner_views.xml',
        'views/assets.xml',
    ],
    'installable': True,
}
