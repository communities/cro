# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from odoo.addons.website.models.website import slug
from odoo.exceptions import UserError, ValidationError

class ResPartnerGrade(models.Model):
    _name = 'res.partner.grade'

    website_published = fields.Boolean(default=True)
    sequence = fields.Integer('Sequence')
    active = fields.Boolean('Active', default=lambda *args: 1)
    name = fields.Char('Designação', translate=True)
   

class ResPartnerWebsite(models.Model):
    _inherit = "res.partner"

    grade_id = fields.Many2one('res.partner.grade', 'Tipo de Parceiro')
    social_facebook = fields.Char('Facebook')
    social_twitter = fields.Char('Twitter')
    social_linkedin = fields.Char('LinkedIn')
    social_youtube = fields.Char('Youtube')
    
#    @api.constrains('website_short_description')
#    @api.one
#    def _check_short_description_field(self):
#        if len(self.website_short_description) > 620:
#            raise ValidationError('O número de caracteres no campo Informação no Website excede os 600 permitidos.')