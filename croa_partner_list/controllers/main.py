# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import datetime
import werkzeug

from collections import OrderedDict

from odoo import fields
from odoo import http
from odoo.http import request
from odoo.addons.website.models.website import slug, unslug
from odoo.addons.website_partner.controllers.main import WebsitePartnerPage
from odoo.tools.translate import _

from odoo.addons.website_portal.controllers.main import website_account



class WebsitePartnerList(WebsitePartnerPage):
    _references_per_page = 8

    @http.route([
        #'/partners',

        #'/partners/state/<model("res.country.state"):state>',
        '/partners',
        '/partners/page/<int:page>',

        '/partners/grade/<model("res.partner.grade"):grade>',
        '/partners/grade/<model("res.partner.grade"):grade>/page/<int:page>',

        '/partners/state/<model("res.country.state"):state>',
        '/partners/state/<model("res.country.state"):state>/page/<int:page>',

        '/partners/grade/<model("res.partner.grade"):grade>/state/<model("res.country.state"):state>',
        '/partners/grade/<model("res.partner.grade"):grade>/state/<model("res.country.state"):state>/page/<int:page>',
    ], type='http', auth="public", website=True)
    def partners(self, state=None, grade=None, page=0, **post):
        state_all = post.pop('state_all', False)
        partner_obj = request.env['res.partner']
        state_obj = request.env['res.country.state']
        search = post.get('search', '')

        base_partner_domain = [('is_company', '=', True), ('grade_id', '!=', False), ('website_published', '=', True)]
        if not request.env['res.users'].has_group('website.group_website_publisher'):
            base_partner_domain += [('grade_id.website_published', '=', True)]
        if search:
            base_partner_domain += ['|', ('name', 'ilike', search), ('website_description', 'ilike', search)]

        # group by grade
        grade_domain = list(base_partner_domain)
        if not state and not state_all:
            state_code = request.session['geoip'].get('state_code')
            if state_code:
                state = state_obj.search([('code', '=', state_code)], limit=1)
        if state:
            grade_domain += [('state_id', '=', state.id)]
        grades = partner_obj.sudo().read_group(
            grade_domain, ["id", "grade_id"],
            groupby="grade_id", orderby="grade_id DESC")
        grades_partners = partner_obj.sudo().search_count(grade_domain)
        # flag active grade
        for grade_dict in grades:
            grade_dict['active'] = grade and grade_dict['grade_id'][0] == grade.id
        grades.insert(0, {
            'grade_id_count': grades_partners,
            'grade_id': (0, _("Todos os tipos")),
            'active': bool(grade is None),
        })


        # group by state
        state_domain = list(base_partner_domain)
        if grade:
            state_domain += [('grade_id', '=', grade.id)]
        states = partner_obj.sudo().read_group(
            state_domain, ["id", "state_id"],
            groupby="state_id", orderby="state_id")
        states_partners = partner_obj.sudo().search_count(state_domain)
        # flag active state
        for state_dict in states:
            state_dict['active'] = state and state_dict['state_id'] and state_dict['state_id'][0] == state.id
        states.insert(0, {
            'state_id_count': states_partners,
            'state_id': (0, _("Todos os Distritos")),
            'active': bool(state is None),
        })

        # current search
        if grade:
            base_partner_domain += [('grade_id', '=', grade.id)]
        if state:
            base_partner_domain += [('state_id', '=', state.id)]

        # format pager
        if grade and not state:
            url = '/partners/grade/' + slug(grade)
        elif state and not grade:
            url = '/partners/state/' + slug(state)
        elif state and grade:
            url = '/partners/grade/' + slug(grade) + '/state/' + slug(state)
        
        # format pager
        if grade and not state:
            url = '/partners/grade/' + slug(grade)
        elif state and not grade:
            url = '/partners/state/' + slug(state)
        elif state and grade:
            url = '/partners/grade/' + slug(grade) + '/state/' + slug(state)
        else:
            url = '/partners'
        url_args = {}
        if search:
            url_args['search'] = search
        if state_all:
            url_args['state_all'] = True

        partner_count = partner_obj.sudo().search_count(base_partner_domain)
        pager = request.website.pager(
            url=url, total=partner_count, page=page, step=self._references_per_page, scope=7,
            url_args=url_args)

        # search partners matching current search parameters
        partner_ids = partner_obj.sudo().search(
            base_partner_domain, order="grade_id DESC, display_name ASC")  # todo in trunk: order="grade_id DESC, implemented_count DESC", offset=pager['offset'], limit=self._references_per_page
        partners = partner_ids.sudo()
        # remove me in trunk
        #partners = sorted(partners, key=lambda x: (x.id if x.id else 1, len([i for i in x.partner_ids if i.website_published])), reverse=True)
        #partners = sorted(partners, key=lambda x: (x.grade_id.sequence if x.grade_id else 0, len([i for i in x.implemented_partner_ids if i.website_published])), reverse=True)
        partners = partners[pager['offset']:pager['offset'] + self._references_per_page]


        values = {
            'states': states,
            'current_state': state,
            'grades': grades,
            'current_grade': grade,
            'partners': partners,
            'pager': pager,
            'searches': post,
            'search_path': "%s" % werkzeug.url_encode(post),
        }
        return request.render("croa_partner_list.index", values, status=partners and 200 or 404)
