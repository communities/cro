# -*- coding: utf-8 -*-
{
    'name': "Centro de Recolha Oficial de Animais - Relatórios",

    'summary': """
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Communities - Comunicações, Lda",
    'website': "http://www.cro.pt",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail'],

    # always loaded
    'data': [
        'report/process_report_templates.xml',
    ],

}