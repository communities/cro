odoo.define('web_cro_dashboard', function (require) {
"use strict";

var core = require('web.core');
var Widget = require('web.Widget');
var Model = require('web.Model');
var session = require('web.session');
var framework = require('web.framework');
var webclient = require('web.web_client');

var QWeb = core.qweb;
var _t = core._t;

var Dashboard = Widget.extend({
    template: 'DashboardCro',

    init: function(parent, data){
        this.all_dashboards = ['animals', 'processes', 'processes_out', 'cro_cheque'];
        return this._super.apply(this, arguments);
    },

    start: function(){
        return this.load(this.all_dashboards);
    },

    load: function(dashboards){
        var self = this;
        var loading_done = new $.Deferred();
        session.rpc("/web_cro_dashboard/data", {}).then(function (data) {
            // Load each dashboard
            var all_dashboards_defs = [];
            _.each(dashboards, function(dashboard) {
                var dashboard_def = self['load_' + dashboard](data);
                if (dashboard_def) {
                    all_dashboards_defs.push(dashboard_def);
                }
            });

            // Resolve loading_done when all dashboards defs are resolved
            $.when.apply($, all_dashboards_defs).then(function() {
                loading_done.resolve();
            });
        });
        return loading_done;
    },

    load_animals: function(data){
        return  new DashboardCroAnimal(this, data.animals).replace(this.$('.o_web_cro_dashboard_animals'));
    },

    load_processes: function(data){
        return new DashboardCroProcesses(this, data.processes).replace(this.$('.o_web_cro_dashboard_processes'));
    },

    load_processes_out: function(data){
        return new DashboardCroProcessesOut(this, data.processes_out).replace(this.$('.o_web_cro_dashboard_processes_out'));
    },

    load_cro_cheque: function(data){
        return new DashboardCroCheque(this, data.cro_cheque).replace(this.$('.o_web_cro_dashboard_cheque'));
    },
});




var DashboardCroAnimal = Widget.extend({

    template: 'DashboardCroAnimal',

    events: {
        'click .o_browse_animal': 'on_new_animal',
        'click .o_browse_box': 'on_new_box',
    },

    init: function(parent, data){
        this.data = data;
        this.parent = parent;
        return this._super.apply(this, arguments);
    },

    on_new_animal: function(){
        this.do_action('cro_core.action_animal_form');
    },

    on_new_box: function(){
        this.do_action('cro_core.action_animal_box_form');
    },
});


var DashboardCroProcesses = Widget.extend({

    template: 'DashboardCroProcesses',

    events: {
        'click .o_browse_process': 'on_new_process',
    },

    init: function(parent, data){
        this.data = data;
        this.parent = parent;
        return this._super.apply(this, arguments);
    },

    on_new_process: function(){
        this.do_action('cro_core.action_processo_cr_form');
    },
});


var DashboardCroProcessesOut = Widget.extend({

    template: 'DashboardCroProcessesOut',

    events: {
        'click .o_browse_process_out': 'on_new_process_out',
    },

    init: function(parent, data){
        this.data = data;
        this.parent = parent;
        return this._super.apply(this, arguments);
    },

    on_new_process_out: function(){
        this.do_action('cro_core.action_processo_out_form');
    },
});

var DashboardCroCheque = Widget.extend({
    template: 'DashboardCroCheque',

    events: {
        'click .o_browse_cheques': 'on_new_cheques',
    },

    init: function(parent, data){
        this.data = data;
        this.parent = parent;
        return this._super.apply(this, arguments);
    },

    on_new_cheques: function(){
        this.do_action('cro_cheque.action_cro_cheque_form');
    },

});


core.action_registry.add('web_cro_dashboard.main', Dashboard);

return {
    Dashboard: Dashboard,
    DashboardCroCheque: DashboardCroCheque,
};

});
