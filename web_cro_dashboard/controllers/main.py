# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, timedelta

from odoo import fields, http
from odoo.exceptions import AccessError
from odoo.http import request
from odoo import release

class WebSettingsCroDashboard(http.Controller):

    @http.route('/web_cro_dashboard/data', type='json', auth='user')
    def web_cro_dashboard_data(self, **kw):
        if not request.env.user.has_group('cro_core.group_cro_back_office'):
            raise AccessError("Access Denied")

        resident_animal = request.env['cro.animal'].search_count([
            ('arquivo_boolean', '=', False),
            ('state', '=', 'resident')
        ])
        
        resident_animal_femea = request.env['cro.animal'].search_count([
            ('arquivo_boolean', '=', False),
            ('state', '=', 'resident'),
            ('sexo', '=', 'femea')
        ])
        
        resident_animal_macho = request.env['cro.animal'].search_count([
            ('arquivo_boolean', '=', False),
            ('state', '=', 'resident'),
            ('sexo', '=', 'macho')
        ])
        
        resident_animal_outro = request.env['cro.animal'].search_count([
            ('arquivo_boolean', '=', False),
            ('state', '=', 'resident'),
            ('sexo', '=', 'outro')
        ])
        
        if resident_animal > 0:
            animal_femea_percent = resident_animal_femea * 100 / resident_animal
            animal_macho_percent = resident_animal_macho * 100 / resident_animal
            animal_outro_percent = resident_animal_outro * 100 / resident_animal
        
        else:
           animal_femea_percent = 0
           animal_macho_percent = 0
           animal_outro_percent = 0    	

        processes_in = request.env['cro.processo.cr'].search_count([
            ('state', 'in', ['confirm', 'done'])
        ])
        
        processes_in_entregadetentor = request.env['cro.processo.cr'].search_count([
            ('motivo', '=', 'entregadetentor'),
            ('state', 'in', ['confirm', 'done'])
        ])
        
        processes_in_entreganaodetentor = request.env['cro.processo.cr'].search_count([
            ('motivo', '=', 'entreganaodetentor'),
            ('state', 'in', ['confirm', 'done'])
        ])
        
        processes_in_entregaautoridade = request.env['cro.processo.cr'].search_count([
            ('motivo', '=', 'entregaautoridade'),
            ('state', 'in', ['confirm', 'done'])
        ])
        
        processes_in_entregaentidaderodoviaria = request.env['cro.processo.cr'].search_count([
            ('motivo', '=', 'entregaentidaderodoviaria'),
            ('state', 'in', ['confirm', 'done'])
        ])
        
        processes_in_recolhaerrante = request.env['cro.processo.cr'].search_count([
            ('motivo', '=', 'recolhaerrante'),
            ('state', 'in', ['confirm', 'done'])
        ])
        
        processes_in_recolhaparticular = request.env['cro.processo.cr'].search_count([
            ('motivo', '=', 'recolhaparticular'),
            ('state', 'in', ['confirm', 'done'])
        ])
        
        processes_in_sequestro = request.env['cro.processo.cr'].search_count([
            ('motivo', '=', 'sequestro'),
            ('state', 'in', ['confirm', 'done'])
        ])
        
        if processes_in > 0:
            processes_in_entregadetentor_percent = processes_in_entregadetentor * 100 / processes_in
            processes_in_entreganaodetentor_percent = processes_in_entreganaodetentor * 100 / processes_in
            processes_in_entregaautoridade_percent = processes_in_entregaautoridade * 100 / processes_in
            processes_in_entregaentidaderodoviaria_percent = processes_in_entregaentidaderodoviaria * 100 / processes_in
            processes_in_recolhaerrante_percent = processes_in_recolhaerrante * 100 / processes_in
            processes_in_recolhaparticular_percent = processes_in_recolhaparticular * 100 / processes_in
            processes_in_sequestro_percent = processes_in_sequestro * 100 / processes_in
        
        else:
            processes_in_entregadetentor_percent = 0
            processes_in_entreganaodetentor_percent = 0
            processes_in_entregaautoridade_percent = 0
            processes_in_entregaentidaderodoviaria_percent = 0
            processes_in_recolhaerrante_percent = 0
            processes_in_recolhaparticular_percent = 0
            processes_in_sequestro_percent = 0      

 
        processes_out = request.env['cro.processo.out'].search_count([
            ('state', 'in', ['confirm', 'done'])
        ]) 
        
        processes_out_adopcao = request.env['cro.processo.out'].search_count([
            ('motivo', '=', 'adopcao'),
            ('state', 'in', ['confirm', 'done'])
        ]) 
        
        processes_out_devolucao = request.env['cro.processo.out'].search_count([
            ('motivo', '=', 'devolucao'),
            ('state', 'in', ['confirm', 'done'])
        ]) 
        
        processes_out_morte = request.env['cro.processo.out'].search_count([
            ('motivo', '=', 'morte'),
            ('state', 'in', ['confirm', 'done'])
        ]) 
        
        processes_out_eutanasia = request.env['cro.processo.out'].search_count([
            ('motivo', '=', 'eutanasia'),
            ('state', 'in', ['confirm', 'done'])
        ]) 
        
        processes_out_entregaassociacao = request.env['cro.processo.out'].search_count([
            ('motivo', '=', 'entregaassociacao'),
            ('state', 'in', ['confirm', 'done'])
        ])
        
        if processes_out > 0:
            processes_out_adopcao_percent = processes_out_adopcao * 100 / processes_out
            processes_out_devolucao_percent = processes_out_devolucao * 100 / processes_out
            processes_out_morte_percent = processes_out_morte * 100 / processes_out
            processes_out_eutanasia_percent = processes_out_eutanasia * 100 / processes_out
            processes_out_entregaassociacao_percent = processes_out_entregaassociacao * 100 / processes_out
        
        else:
            processes_out_adopcao_percent = 0
            processes_out_devolucao_percent = 0
            processes_out_morte_percent = 0
            processes_out_eutanasia_percent = 0
            processes_out_entregaassociacao_percent = 0   

        cheque_total = request.env['cro.cheque'].search_count([
            ('state', 'in', ['confirm', 'done', 'print'])
        ])
        
        credito_sum = 0.0
        credito = request.env['cro.account.move.line'].search([
            ('partner_id', '=', '4')
        ])
        
        credito_sum += credito.credit
        
        credito_disponivel = credito_sum

        return {
            'animals': {
                'resident_animal': resident_animal,
                'animal_femea_percent': animal_femea_percent,
                'animal_macho_percent': animal_macho_percent,
                'animal_outro_percent': animal_outro_percent,
            },
            'processes': {
                'processes_in': processes_in,
                'processes_in_entregadetentor_percent': processes_in_entregadetentor_percent,
                'processes_in_entreganaodetentor_percent': processes_in_entreganaodetentor_percent,
                'processes_in_entregaautoridade_percent': processes_in_entregaautoridade_percent,
                'processes_in_entregaentidaderodoviaria_percent': processes_in_entregaentidaderodoviaria_percent,
                'processes_in_recolhaerrante_percent': processes_in_recolhaerrante_percent,
                'processes_in_recolhaparticular_percent': processes_in_recolhaparticular_percent,
                'processes_in_sequestro_percent': processes_in_sequestro_percent,      
            },
            'processes_out': {
                'processes_out': processes_out,
                'processes_out_adopcao_percent': processes_out_adopcao_percent,
                'processes_out_devolucao_percent': processes_out_devolucao_percent,
                'processes_out_morte_percent': processes_out_morte_percent,
                'processes_out_eutanasia_percent': processes_out_eutanasia_percent,
                'processes_out_entregaassociacao_percent': processes_out_entregaassociacao_percent,
            },
            'cro_cheque': {
                'cheque_total': cheque_total,
                'credito_disponivel': credito_disponivel,
                'server_version': release.version,
                'debug': request.debug,
            }
        }
