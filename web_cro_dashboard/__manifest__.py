# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'CRO Dashboard',
    'version': '1.0',
    'summary': 'Quick actions and views, etc.',
    'category': 'Extra Tools',
    'description':
    """
CRO dashboard
        """,
    'data': [
        'views/dashboard_views.xml',
        'views/dashboard_templates.xml',
    ],
    'depends': ['web_planner', 'cro_core', 'cro_cheque'],
    'qweb': ['static/src/xml/dashboard.xml'],
    'auto_install': True,
}
