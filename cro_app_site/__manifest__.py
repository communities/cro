{
    # Theme information
    'name': "cro_app",
    'description': """
    """,
    'category': 'Theme',
    'version': '1.0',
    'depends': ['website', 'crm', 'website_crm', 'contacts'],

    # templates
    'data': [
        'views/oerp_saas_server_templates.xml',
        'views/saas_database_views.xml',
        'views/saas_template_database_views.xml',
        'views/res_partner_views.xml',
        'views/assets.xml',
        'views/homepage.xml',
        'views/error_page.xml',
        'views/template.xml',
        'data/ir.cron.csv',
        'data/saas.modules.builtin.csv',
        'data/res.partner.category.csv',
        'data/res.groups.csv',
        'data/website.menu.csv',
        'data/ir.rule.csv',
        'security/ir.model.access.csv',
    ],

    # demo pages
    'demo': [
        'demo/pages.xml',
    ],

    # Your information
    'author': "Communities - Comunicações, Lda",
    'website': "https://www.communities.pt",
}