# -*- coding: utf-8 -*-
from openerp import api, fields, models

class ResPartnerDirectory(models.Model):

    _inherit = "res.partner"

    in_directory = fields.Boolean(string="Elegível")
    dbname = fields.Char(string="Base de Dados")
    company_category_ids = fields.Many2many('res.partner.directory.category', string="Directory Categories")

class ResPartnerDirectoryCategory(models.Model):

    _name = "res.partner.directory.category"

    parent_category = fields.Many2one('res.partner.directory.category', string="Parent Category")
    children_category_ids = fields.One2many('res.partner.directory.category', 'parent_category', string="Child Categories")
    name = fields.Char(string="Category Name")