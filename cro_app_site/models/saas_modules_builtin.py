# -*- coding: utf-8 -*-
import logging
_logger = logging.getLogger(__name__)
from datetime import datetime, timedelta

from odoo import api, fields, models
import odoo.http as http
from odoo.http import request

class SaasModulesBuiltin(models.Model):

    _name = "saas.modules.builtin"
    
    name = fields.Char(string="Database Name")