# -*- coding: utf-8 -*-
import logging
_logger = logging.getLogger(__name__)
from datetime import datetime, timedelta

from odoo import api, fields, models
import odoo.http as http
from odoo.http import request

class AttachmentSaasDatabase(models.Model):

    _inherit = "ir.attachment"
    
    saas_database_id = fields.Many2one(string="SAAS Datebase")