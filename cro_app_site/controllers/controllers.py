# -*- coding: utf-8 -*-
from odoo import http, _
from odoo.exceptions import AccessError
from odoo.http import request

class WebsiteController(http.Controller):
	
    @http.route('/page/homepage', type="http", auth="public", website=True)
    def croa_clients(self, **kwargs):
        organizations = http.request.env['res.partner'].sudo().search([('in_directory','=', True)])
        template_databases = request.env['saas.template.database'].search([])

        
        return http.request.render('website.homepage', {'organizations': organizations, 'template_databases': template_databases} )
        
        
    @http.route('/page/db_erro', type="http", auth="public", website=True)
    def croa_db_erro(self, **kwargs):
        organizations = http.request.env['res.partner'].sudo().search([('in_directory','=', True)])
        
        return http.request.render('cro_app_site.error_page', {'organizations': organizations} )
        
        
    @http.route('/page/user_erro', type="http", auth="public", website=True)
    def croa_user_erro(self, **kwargs):
        organizations = http.request.env['res.partner'].sudo().search([('in_directory','=', True)])
        
        return http.request.render('cro_app_site.user_error_page', {'organizations': organizations} )