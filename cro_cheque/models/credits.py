# -*- coding: utf-8 -*-

import xmlrpclib
import string
import random

from odoo import api, fields, models, tools, _
from odoo.modules import get_module_resource
from odoo.osv.expression import get_unaccent_wrapper
from odoo.exceptions import UserError, ValidationError
from odoo.osv.orm import browse_record
import odoo.addons.decimal_precision as dp
 

#
# Creditos #####################################################################
#  
       
    
class CroCredit(models.Model):
    _description = 'Creditos de CROs'
    _name = "cro.credit"
    _inherit = ['mail.thread']

    name = fields.Char(string='Code', required=True, copy=False, readonly=True, index=True, default=lambda self: _('Rascunho'))
    ref = fields.Char(string='Referencia', copy=False)
    data = fields.Date(string='Data do Movimento', required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    croa = fields.Many2one('res.company', string='CROA', required=True, domain="[('partner_type', '=', 'croa'), ('active', '=', True)]")
    total = fields.Float(string="Valor a Creditar", required=True, copy=False)
    doc_payment = fields.Binary(string="Comprovativo", required=True, attachment=True)
    doc_payment_name = fields.Char('Nome do Ficheiro')    
    # Movimentos Contabilisticos
    move_id = fields.Many2one('cro.account.move', 'Accounting Entry', readonly=True, copy=False)

    state = fields.Selection(
        [('draft', 'Rascunho'), ('confirm', 'Confirmado'),
         ('cancel', 'Cancelado')],
        'State', readonly=True, index=True,
        default='draft', track_visibility='onchange')


    @api.one
    def confirm_in_progress(self):
        self.state = 'confirm'
        if self.name == _('Rascunho'):
            self.name = self.env['ir.sequence'].next_by_code('cro.credit.serial') or _('Rascunho')

####### Movimentos

        for ck in self:
            line_ids = []
            data = ck.data

            name = _('Credito %s') % (ck.name)
            move_dict = {
                'ref': name,
                'data': ck.data,
            }
            
 #           debit_line = (0, 0, {
 #              'name': ck.name,
 #              'partner_id': ck.company_id.id,
 #              'data': ck.data,
 #              'debit': ck.total,
 #              'credit': 0.0,
 #           })
 #           line_ids.append(debit_line)   
            
            credit_line = (0, 0, {
               'name': ck.name,
               'partner_id': ck.croa.id,
               'data': ck.data,
               'debit': 0.0,
               'credit': ck.total,
            })
            line_ids.append(credit_line)        
            
            move_dict['line_ids'] = line_ids
            move = self.env['cro.account.move'].create(move_dict)
            ck.write({'move_id': move.id, 'data': data})
            move.filtered(lambda x: x.state == 'draft').confirm_in_progress()

######


    @api.one
    def confirm_to_draft(self):
        self.state = 'draft'

    @api.one
    def confirm_cancel(self):
        self.state = 'cancel'
        self.animal.state = 'resident'
        self.animal.arquivo_boolean = False
        moves = self.mapped('move_id')
        moves.filtered(lambda x: x.state == 'posted').button_cancel()
        moves.sudo().unlink()

    @api.multi
    def print_process(self):
        return self.env['report'].get_action(self, 'cro_cheque.report_cv')

    @api.multi
    def copy(self):
        raise ValidationError(_("Não é possível duplicar este registo"))

    @api.multi
    def unlink(self):
        if self.name != _('Rascunho'):
            raise ValidationError(_("Não é possível eliminar um crédito confirmado"))
        return super(CroCheque, self).unlink()   
           

