# -*- coding: utf-8 -*-
# Copyright 2016, 2017 Openworx
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

from odoo import models, api, fields

class ResCompany(models.Model):

    _inherit = 'res.company'
    
    @api.model
    def get_omv(self):
        return self.env['res.company'].search([('partner_type', 'in', ['omv'])]).ids
    
    partner_type = fields.Selection(string="Tipo de Parceiro", required=True,
        selection=[('croa', 'CROA'), ('camv', 'CAMV'), ('omv', 'OMV'), ('outro', 'Outros')])
    grade_id = fields.Many2one('res.partner.grade', 'Tipo de Parceiro', related='partner_id.grade_id')
    website_published = fields.Boolean('Publicar no Website', related='partner_id.website_published')
    website_short_description = fields.Text('Website Partner Short Description', related='partner_id.website_short_description')
    social_facebook = fields.Char('Facebook', related='partner_id.social_facebook')
    social_twitter = fields.Char('Twitter', related='partner_id.social_twitter')
    social_linkedin = fields.Char('LinkedIn', related='partner_id.social_linkedin')
    social_youtube = fields.Char('Youtube', related='partner_id.social_youtube')
    camv_type = fields.Selection(string="Tipologia",
        selection=[('Hospital', 'Hospital'), ('Clínica', 'Clínica'), ('Consultório', 'Consultório')])
    id_omv = fields.Char(string='OMV ID')
    active = fields.Boolean(string="Subscrição Activa", default=True)
    iban = fields.Char(related='partner_id.iban', string='IBAN')
    cheque_line_tax_ids = fields.Many2one('cro.taxa.cheque', string='Taxa Administrativa', domain="[('tipo_taxa', '=', 'ta')]")
    cheque_tipo_ids = fields.Many2many('cro.tipo.cheque', 'res_company_cheque_tipo_rel', 'company_id', 'tipo_id', string="Tipo de Cheques", copy=False)
    
    parent_id = fields.Many2many('res.company', 'res_company_parent_rel', 'c1_id', 'c2_id', string='Parent Company', default=get_omv, index=True, copy=False)
    child_ids = fields.Many2many('res.company', 'res_company_parent_rel', 'c2_id', 'c1_id', string='Child Companies', copy=False)

    @api.constrains('partner_type')
    @api.one
    def _onchange_partner_type(self):
        if not self.partner_type:
            return
        elif self.partner_type == 'croa':
            self.grade_id = 1
        elif self.partner_type == 'camv':
            self.grade_id = 2
        else:
            self.grade_id = ''
    
    @api.multi
    def website_publish_button(self):
        self.ensure_one()
        return self.partner_id.website_publish_button()