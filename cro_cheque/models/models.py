# -*- coding: utf-8 -*-

import xmlrpclib
import string
import random

from odoo import api, fields, models, tools, _
from odoo.modules import get_module_resource
from odoo.osv.expression import get_unaccent_wrapper
from odoo.exceptions import UserError, ValidationError
from odoo.osv.orm import browse_record
import odoo.addons.decimal_precision as dp

#
# Add cheque button to tmv proccess #####################################################################
#

class CroTmvToCheque(models.Model):

    _inherit = "cro.processo.tmv"
    
    cheque = fields.One2many('cro.cheque', 'tmv_id', 'Cheque')
    no_of_cheque = fields.Integer('Cheque', compute="_compute_cheque")

    @api.multi    
    def open_tmv_cheque_veterinario(self):

        tmv_id = self.id

        res = {
            'type': 'ir.actions.act_window',
            'name': _('Cheque Veterinario'),
            'res_model': 'cro.cheque',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'current',
            'context': {'default_tmv_id': tmv_id}
        }
        
        return res        

    @api.multi
    def action_view_cheque(self):
        cheques = self.mapped('cheque')
        action = self.env.ref('cro_cheque.action_cro_cheque_form').read()[0]
        if len(cheques) > 1:
            action['domain'] = [('id', 'in', cheques.ids)]
        elif len(cheques) == 1:
            action['views'] = [(self.env.ref('cro_cheque.view_cro_cheque_form').id, 'form')]
            action['res_id'] = cheques.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    @api.multi
    def _compute_cheque(self):
        cheque_list = []
        for res in self:
            for ck in res.cheque:
                cheque_list.append(ck.id)
            cheque_len = len(cheque_list)
            res.no_of_cheque = cheque_len
        return cheque_len
#
# Add cheque button to adoption proccess #####################################################################
#

class CroProcessosToCheque(models.Model):

    _inherit = "cro.processo.out"
    
    cheque = fields.One2many('cro.cheque', 'processo_id', 'Cheque')
    no_of_cheque = fields.Integer('Cheque', compute="_compute_cheque")

    @api.multi    
    def open_cheque_veterinario(self):

        processo_id = self.id

        res = {
            'type': 'ir.actions.act_window',
            'name': _('Cheque Veterinario'),
            'res_model': 'cro.cheque',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'current',
            'context': {'default_processo_id': processo_id}
        }
        
        return res        

    @api.multi
    def action_view_cheque(self):
        cheques = self.mapped('cheque')
        action = self.env.ref('cro_cheque.action_cro_cheque_form').read()[0]
        if len(cheques) > 1:
            action['domain'] = [('id', 'in', cheques.ids)]
        elif len(cheques) == 1:
            action['views'] = [(self.env.ref('cro_cheque.view_cro_cheque_form').id, 'form')]
            action['res_id'] = cheques.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    @api.multi
    def _compute_cheque(self):
        cheque_list = []
        for res in self:
            for ck in res.cheque:
                cheque_list.append(ck.id)
            cheque_len = len(cheque_list)
            res.no_of_cheque = cheque_len
        return cheque_len

#
# Taxas e Impostos #####################################################################
#         

class CroTaxa(models.Model):
    _description = 'Taxas Administrativas'
    _name = "cro.taxa.cheque"

    name = fields.Char(size=64, string='Designação', required=True)
    tipo_taxa = fields.Selection([('iva',' IVA'), ('ta',' Taxas Administrativas')], default="ta")
    amount_type = fields.Selection(default='percent', string="Tipo de Calculo", required=True,
        selection=[('fixed', 'Valor Fixo'), ('percent', 'Percentagem do Preço')])
    amount = fields.Float(required=True, digits=(16, 4))

#
# Tipo de Cheques #####################################################################
#

class CroTipoCheque(models.Model):
    _description = 'Tipo de Cheque'
    _name = "cro.tipo.cheque"

    name = fields.Char(size=64, string='Designação')
    code = fields.Char(size=16, string='Código', index=True)
    price = fields.Float(string='Valor do Cheque', required=True,)
    iva = fields.Many2one('cro.taxa.cheque', string='Taxas', required=True, domain="[('tipo_taxa', '=', 'iva')]")

#
# Tipo de Vaina #####################################################################
#

class CroTipoVacina(models.Model):
    _description = 'Tipo de Vacina'
    _name = "cro.tipo.vacina"

    name = fields.Char(size=64, string='Designação')
    code = fields.Char(size=16, string='Código', index=True)

#
# Tipo de Cirurgia #####################################################################
#

class CroTipoCirurgia(models.Model):
    _description = 'Tipo de Cirurgia'
    _name = "cro.tipo.cirurgia"

    name = fields.Char(size=64, string='Designação')
    code = fields.Char(size=16, string='Código', index=True)

#
# Cheque Lines #####################################################################
#

class CroChequeLine(models.Model):
    _name = "cro.cheque.line"

    name = fields.Char(string='Código')
    company_id = fields.Many2one('res.company', string='CRO', required=True, default=lambda self: self.env.user.company_id)
    camv = fields.Many2one('res.company', string='CAMV', required=True) #################3
    tipo_id = fields.Many2one('cro.tipo.cheque', string='Tipo de Cheque',
        ondelete='restrict', index=True)
    cheque_id = fields.Many2one('cro.cheque', string='Cheque Reference',
        ondelete='cascade', index=True)
    quantidade = fields.Integer(string='Quantidade', required=True, default=1)
    cheque_line_tax_ids = fields.Many2one('cro.taxa.cheque', string='Taxa Administrativa')
    price_unit = fields.Float(string='Valor Unitario', required=True)
    taxa = fields.Float(string='TA', store=True, readonly=True, compute='_compute_taxa')
    iva = fields.Float(string='IVA', store=True, readonly=True, compute='_compute_iva')
    price_subtotal = fields.Float(string='Total',
        store=True, readonly=True, compute='_compute_price')

    @api.onchange('tipo_id')
    def _onchange_tipo_id(self):
        if not self.tipo_id:
            return
        tipo = self.tipo_id
        taxa = self.company_id
        camv = self.cheque_id
        self.price_unit = tipo.price
        self.name = tipo.code
        self.cheque_line_tax_ids = taxa.cheque_line_tax_ids
        self.camv = camv.camv    

    @api.one
    @api.depends('cheque_id', 'price_unit', 'quantidade')
    def _compute_iva(self):
        for p in self:
            p.iva = (self.price_unit * self.tipo_id.iva.amount) * self.quantidade 

    @api.one
    @api.depends('cheque_line_tax_ids', 'price_unit', 'quantidade')
    def _compute_taxa(self):
        for p in self:
            if self.cheque_line_tax_ids.amount_type == 'percent':
                p.taxa = (self.price_unit * self.cheque_line_tax_ids.amount) * self.quantidade
            if self.cheque_line_tax_ids.amount_type == 'fixed':
                p.taxa = self.cheque_line_tax_ids.amount * self.quantidade
        
    @api.one
    @api.depends('price_unit', 'quantidade')
    def _compute_price(self):
        for p in self:
            p.price_subtotal = self.price_unit * self.quantidade      

#
# Cheque Veterinario #####################################################################
#  
       
    
class CroCheque(models.Model):
    _description = 'Cheque Veterinario'
    _name = "cro.cheque"
    _inherit = ['mail.thread']

    def _compute_price(self):
        for p in self:
            p.subtotal = 0.0	
            for line in p.cheque_line_ids:
                p.subtotal += line.price_subtotal
                
    def _compute_taxa(self):
        for p in self:
            p.taxa = 0.0	
            for line in p.cheque_line_ids:
                p.taxa += line.taxa  
                
    def _compute_iva(self):
        for p in self:
            p.iva = 0.0	
            for line in p.cheque_line_ids:
                p.iva += line.iva      

    @api.one
    @api.depends('subtotal')       
    def _compute_total(self):
        for p in self:
            p.total = self.subtotal + self.iva
                
    name = fields.Char(string='Code', required=True, copy=False, readonly=True, index=True, default=lambda self: _('Rascunho')) 
    code = fields.Char(string='Codigo de Validação', required=True, copy=False, readonly=True, index=True, default=lambda self: _('code'))
    validate = fields.Char(string='Codigo de Validação', copy=False)  
    processo_id = fields.Many2one('cro.processo.out', string='Processo', domain="[('state', '=', 'done'), ('motivo', '=', 'adopcao')]")  
    tmv_id = fields.Many2one('cro.processo.tmv', string='Processo', domain="[('state', '=', 'done'), ('motivo', '=', 'externo')]")
    
    cheque_line_ids = fields.One2many('cro.cheque.line', 'cheque_id', string='Cheque Lines',
        readonly=True, states={'draft': [('readonly', False)]}, copy=True)
    taxa = fields.Float(compute='_compute_taxa', string="Taxa Administrativa")
    iva = fields.Float(compute='_compute_iva', string="IVA")    
    subtotal = fields.Float(compute='_compute_price', string="Subtotal")
    total = fields.Float(compute='_compute_total', string="Valor do Cheque")
    data = fields.Date(string='Data de Emissão', required=True)
    camv = fields.Many2one('res.company', string='CAMV', required=True, domain="[('partner_type', '=', 'camv'), ('active', '=', True)]")
    camv_mv = fields.Many2one('res.users', string='Médico Veterinário', required=True, domain="[('company_id', '=', camv)]")
    mv = fields.Many2one('res.users', string='Médico Veterinário', index=True, readonly=True, track_visibility='onchange', default=lambda self: self.env.user)
    company_id = fields.Many2one('res.company', string='CRO', required=True, default=lambda self: self.env.user.company_id)
    
    # Movimentos Contabilisticos
    move_id = fields.Many2one('cro.account.move', 'Accounting Entry', readonly=True, copy=False)
    
    tutor = fields.Char(size=50, string='Nome do Tutor')
    tutor_cc = fields.Char('BI/CC', size=16, help="Número do BI/CC")
    #animal = fields.Char(size=25, string='Nome do Animal')
    
    animal = fields.Many2one('cro.animal', string='Animal', required=True,  domain="[('state', '!=', 'euthanized'), ('state', '!=', 'death'), ('state', '!=', 'adopted'), ('state', '!=', 'returned')]")    
    
    a_especie = fields.Many2one('cro.animal.especie', string='Espécie')
    a_sexo = fields.Selection([('macho','Macho'), ('femea','Fêmea'), ('outro','Não Definido')], default="outro", string="Sexo") 
    a_peso = fields.Float(string='Peso')
    a_puni = fields.Selection([('grama',' Grama(s)'), ('quilograma',' Quilograma(s)')], default="quilograma")
    a_iuni = fields.Selection([('mes',' Mes(es)'), ('ano',' Ano(s)')], default="ano")
    a_chip = fields.Char(string='Microchip')
    a_idade = fields.Char(string='Idade')
    a_raca = fields.Many2one('cro.animal.raca', string='Raça')
    notas = fields.Text(string='Notas Internas')
    vacina = fields.Many2one('cro.tipo.vacina', string='Tipo de Vacina')
    cirurgia = fields.Many2one('cro.tipo.cirurgia', string='Tipo de Cirurgia')
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id)
    situation = fields.Selection(
        [('refund', 'Reembolsado'), ('refund_request', 'Pedido de Reembolso em Curso')],
        'Situation', readonly=True, index=True, track_visibility='onchange')

    state = fields.Selection(
        [('draft', 'Rascunho'), ('confirm', 'Confirmado'), ('print', 'Impresso'),
         ('cancel', 'Cancelado'), ('done', 'Utilizado')],
        'State', readonly=True, index=True,
        default='draft', track_visibility='onchange')

    @api.onchange('camv')
    def onchange_camv(self):
        self.camv_mv = False
        self.cheque_line_ids = False

    @api.onchange('processo_id')
    def _onchange_processo_id(self):
        if not self.processo_id:
            return
        processo = self.processo_id
        self.tutor = processo.adoptante_id.name
        self.tutor_cc = processo.adoptante_id.cc_number
        self.animal = processo.animal_id
        self.a_especie = processo.animal_id.especie_id
        self.a_sexo = processo.animal_id.sexo
        self.a_peso = processo.animal_id.peso
        self.a_puni = processo.animal_id.puni
        self.a_chip = processo.animal_id.chip
        self.a_idade = processo.animal_id.c_idade
        self.a_raca = processo.animal_id.raca_id

    @api.onchange('tmv_id')
    def _onchange_tmv_id(self):
        if not self.tmv_id:
            return
        processo = self.tmv_id
        self.camv= processo.camv
        self.animal = processo.animal_id
        self.a_especie = processo.animal_id.especie_id
        self.a_sexo = processo.animal_id.sexo
        self.a_peso = processo.animal_id.peso
        self.a_puni = processo.animal_id.puni
        self.a_chip = processo.animal_id.chip
        self.a_idade = processo.animal_id.c_idade
        self.a_raca = processo.animal_id.raca_id
        
        
    @api.onchange('animal')
    def _onchange_animal(self):
        if not self.animal:
            return
        a = self.animal
        self.tutor = a.dono_id.name
        self.tutor_cc = a.dono_id.cc_number
        self.a_especie = a.especie_id
        self.a_sexo = a.sexo
        self.a_peso = a.peso
        self.a_puni = a.puni
        self.a_chip = a.chip
        self.a_idade = a.c_idade
        self.a_raca = a.raca_id         

    @api.constrains('cheque_line_ids')
    @api.one
    def _check_cheque_line(self):
        if not self.cheque_line_ids:
            raise ValidationError("Esta a emitir um cheque sem tipologias!!!!\n"
                                    "Deve adicionar pelo menos uma tipologia de Chegue.\n") 

    @api.constrains('animal')
    @api.one
    def _check_animal_tutor(self):
        if not self.animal.dono_id:
            raise ValidationError("Esta a emitir um cheque para um animal sem tutor!!!!\n"
                                    "edite a ficha do animal e associe um tutor.\n"
                                    "Se o animal for residente o tutor será o próprio CRO.\n")

    @api.one
    def confirm_in_progress(self):
        self.state = 'confirm'
        self.animal.state = 'adopted'
        self.animal.arquivo_boolean = True
        if self.name == _('Rascunho'):
            self.name = self.env['ir.sequence'].next_by_code('cro.cheque.serial') or _('Rascunho')
            self.code = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))

####### Movimentos

        for ck in self:
            line_ids = []
            data = ck.data

            name = _('Vale %s') % (ck.name)
            move_dict = {
                'ref': name,
                'data': ck.data,
            }
            
            debit_line = (0, 0, {
               'name': ck.name,
               'partner_id': ck.company_id.id,
               'data': ck.data,
               'debit': ck.total,
               'credit': 0.0,
            })
            line_ids.append(debit_line)   
            
            credit_line = (0, 0, {
               'name': ck.name,
               'partner_id': ck.camv.id,
               'data': ck.data,
               'debit': 0.0,
               'credit': ck.total,
            })
            line_ids.append(credit_line)        
            
            move_dict['line_ids'] = line_ids
            move = self.env['cro.account.move'].create(move_dict)
            ck.write({'move_id': move.id, 'data': data})
            move.filtered(lambda x: x.state == 'draft').confirm_in_progress()

######


    @api.one
    def confirm_to_draft(self):
        self.state = 'draft'

    @api.one
    def confirm_cancel(self):
        self.state = 'cancel'
        self.animal.state = 'resident'
        self.animal.arquivo_boolean = False
        moves = self.mapped('move_id')
        moves.filtered(lambda x: x.state == 'posted').button_cancel()
        moves.sudo().unlink()


    @api.one
    def confirm_validate(self):
        #self.state = 'done'
        if self.validate == self.code:
            self.state = 'done'
        else:
            raise ValidationError(_("O código de validação está errado"))
 
 
    def open_partner_account(self):
        '''
        Isto é para desenvolver mais tarde #####################################################
        '''
        action = self.env.ref('cro_cheque.action_cro_cheque_form')
        result = action.read()[0]
        #result['domain'] = [('id', 'in', self.ids)]
        return result

    @api.multi
    def print_process(self):
        self.filtered(lambda s: s.state == 'confirm').write({'state': 'print'})
        return self.env['report'].get_action(self, 'cro_cheque.report_cv')

    @api.multi
    def copy(self):
        raise ValidationError(_("Não é possível duplicar este registo"))

    @api.multi
    def unlink(self):
        if self.name != _('Rascunho'):
            raise ValidationError(_("Não é possível eliminar um cheque confirmado"))
        return super(CroCheque, self).unlink()   
           
#
# Reembolsos CAMV #####################################################################
#

class CamvChequeRequest(models.Model): 	
    _description = 'Pedido de Reembolso'
    _name = "cro.cheque.request"
    _inherit = ['mail.thread']

    def _compute_total(self):
        for p in self:
            p.total = 0.0	
            for line in p.cheque_ids:
                p.total += line.total

    name = fields.Char(string='Code', required=True, copy=False, readonly=True, index=True, default=lambda self: _('Rascunho'))
    data = fields.Date(string='Data de Emissão')
    company_id = fields.Many2one('res.company', string='CAMV', required=True, default=lambda self: self.env.user.company_id)
    cheque_ids = fields.Many2many('cro.cheque', 'cro_cheque_request_rel', 'request_id', 'cheque_id', string="Cheques", copy=False, readonly=False, domain="[('camv', '=', company_id), ('state','=','done'), ('situation', 'not in', ['refund', 'refund_request'])]")
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id)        
    total = fields.Float(compute='_compute_total', string="Total a Reembolsar")
    notas = fields.Text(string='Notas Internas')
    doc = fields.Binary(string="Factura", required=True, attachment=True)
    doc_name = fields.Char('Nome do Ficheiro')
    doc_payment = fields.Binary(string="Comprovativo", attachment=True)
    doc_payment_name = fields.Char('Nome do Ficheiro')

    state = fields.Selection(
        [('draft', 'Rascunho'), ('confirm', 'Confirmado'),
         ('cancel', 'Cancelado'), ('wait', 'Aguarda Reembolso'), ('done', 'Reembolsado')],
        'State', readonly=True, index=True,
        default='draft', track_visibility='onchange')    
   
    @api.one
    def confirm_in_progress(self):
        self.state = 'confirm'
        if self.name == _('Rascunho'):
            self.name = self.env['ir.sequence'].next_by_code('cro.cheque.request.serial') or _('Rascunho')
        for c in self:	
            for line in c.cheque_ids:
                if not line.situation:
                    line.situation = 'refund_request'
                else:
                    raise ValidationError(_("Está a solicitar reembolso de cheques já listados em outro reembolso"))

    @api.one
    def confirm_to_draft(self):
        self.state = 'draft'

    @api.one
    def confirm_cancel(self):
        self.state = 'cancel'
        for c in self:	
            for line in c.cheque_ids:
                if line.situation == 'refund_request':
                    line.situation = ''
                elif line.situation == 'refund':
                    raise ValidationError(_("Não pode cancelar um pedido já reembolsado"))

    @api.one
    def confirm_validate(self):
        self.state = 'wait'

    @api.multi
    def print_request(self):
        return self.env['report'].get_action(self, 'cro_cheque.request_report_cv')
        
    @api.one
    def request_done(self):
        self.state = 'done'
 
    @api.multi
    def copy(self):
        raise ValidationError(_("Não é possível duplicar este registo"))

    @api.multi
    def unlink(self):
        if self.name != _('Rascunho'):
            raise ValidationError(_("Não é possível eliminar um registo confirmado"))
        return super(CroCheque, self).unlink()  
         
#
# Movimentos Contabilísticos Linhas#####################################################################
#    

class CroaAccountMovementLine(models.Model): 	
    _description = 'Linhas de Movimentos'
    _name = "cro.account.move.line" ##### aqui pode ser movimento a credito ou débito

    name = fields.Char(string='Code', required=True, copy=False, readonly=True, index=True, default=lambda self: _('Rascunho'))
    data = fields.Date(string='Data do Movimento')
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    partner_id = fields.Many2one('res.company', string='Parceiro', required=True, domain="[('active', '=', True)]")

    debit = fields.Float(default=0.0, string='Débito')
    credit = fields.Float(default=0.0, string='Crédito')

    move_id = fields.Many2one('cro.account.move', string='Conta de Parceiro', ondelete="cascade",
        help="The move of this entry line.", index=True, required=True, auto_join=True)
    ref = fields.Char(related='move_id.ref', string='Reference', store=True, copy=False, index=True)  
 
         
#
# Movimentos Contabilísticos #####################################################################
#  
   
class CroaAccountMovement(models.Model): 	
    _description = 'Movimentos de Conta'
    _name = "cro.account.move" ####  pagamento à OMV
    _inherit = ['mail.thread'] 

    @api.multi
    @api.depends('line_ids.debit', 'line_ids.credit')
    def _amount_compute(self):
        for move in self:
            total = 0.0
            total_d = 0.0
            for line in move.line_ids:
                total += line.credit
                total_d += line.debit
            move.amount = total - total_d

    @api.multi
    @api.depends('line_ids.credit')
    def _credit_compute(self):
        for move in self:
            total = 0.0
            for line in move.line_ids:
                total += line.credit
            move.credit = total
            
    @api.multi
    @api.depends('line_ids.debit')
    def _debit_compute(self):
        for move in self:
            total = 0.0
            for line in move.line_ids:
                total += line.debit
            move.debit = total

    name = fields.Char(string='Code', required=True, copy=False, readonly=True, index=True, default=lambda self: _('Rascunho'))
    ref = fields.Char(string='Referencia', copy=False)
    data = fields.Date(string='Data do Movimento')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    croa_id = fields.Many2one('res.company', string='CROA', domain="[('partner_type', '=', 'croa'), ('active', '=', True)]")
    camv_id = fields.Many2one('res.company', string='CAMV', domain="[('partner_type', '=', 'camv'), ('active', '=', True)]")
    
    line_ids = fields.One2many('cro.account.move.line', 'move_id', string='Linhas de Movimentos', copy=True)
    amount = fields.Float(compute='_amount_compute', store=True)
    debit = fields.Float(compute='_debit_compute')
    credit = fields.Float(compute='_credit_compute')
    state = fields.Selection([('draft', 'Unposted'), ('posted', 'Posted')], string='Status',
      required=True, readonly=True, copy=False, default='draft')

    @api.one
    def confirm_in_progress(self):
        self.state = 'posted'
        if self.name == _('Rascunho'):
            self.name = self.env['ir.sequence'].next_by_code('cro.account.move.serial') or _('Rascunho')   
  
    @api.one
    def button_cancel(self):
        self.state = 'draft'       

