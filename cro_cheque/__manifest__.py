# -*- coding: utf-8 -*-
{
    'name': "Centro de Recolha Oficial de Animais - Cheques",

    'summary': """
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Communities - Comunicações, Lda",
    'website': "http://www.cro.pt",

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'cro_core', 'kanban_draggable', 'barcode_widget', 'inputmask_widget'],

    # always loaded
    'data': [
        'views/cheque_views.xml',
        'views/cheque_camv_views.xml',
        'views/cheque_omv_views.xml',
        'views/cheque_camv_request_views.xml',
        'views/cheque_omv_request_views.xml',
        'views/tipo_cheque_views.xml',
        'views/taxas_cheque_views.xml',
        'views/cro_sequence_data.xml',
        'views/users.xml',
        'views/res_company_view.xml',
        'views/account_move_omv_views.xml',
        'views/account_credit_omv_views.xml',
        'report/cheque_report.xml',
        'report/cheque_report_templates.xml',
        'report/cheque_request_report.xml',
        'report/cheque_request_report_templates.xml',
        'report/report_layout_header.xml',
        'report/report_layout_footer.xml',
        #'report/cheque_list_report_templates.xml',
        'security/cheque_security.xml',
        'security/ir.model.access.csv',
        'data/cro.taxa.cheque.csv',
        'data/cro.tipo.cheque.csv',
    ],
}
