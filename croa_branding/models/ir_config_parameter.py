# -*- coding: utf-8 -*-
from odoo import models, api
from odoo.tools.translate import _

PARAMS = [
    ('croa_branding.new_name', _('CROA')),
    ('croa_branding.new_title', _('CROA')),
    ('croa_branding.new_website', _('croa.pt')),
    ('croa_branding.new_documentation_website', 'https://www.aya.pt'),
    ('croa_branding.favicon_url', 'croa_branding/static/src/img/favicon.ico'),
    ('croa_branding.send_publisher_warranty_url', '0'),
    ('croa_branding.planner_footer', ''),
    ('croa_branding.icon_url', 'croa_branding/static/src/img/favicon_192x192.png'),
    ('croa_branding.apple_touch_icon_url', 'croa_branding/static/src/img/favicon_152x152.png'),

]


class IrConfigParameter(models.Model):
    _inherit = 'ir.config_parameter'

    @api.model
    def get_debranding_parameters(self):
        res = {}
        for param, default in PARAMS:
            value = self.env['ir.config_parameter'].get_param(param, default)
            res[param] = value.strip()
        return res

    @api.model
    def create_debranding_parameters(self):
        for param, default in PARAMS:
            if not self.env['ir.config_parameter'].get_param(param):
                self.env['ir.config_parameter'].set_param(param, default or ' ')
