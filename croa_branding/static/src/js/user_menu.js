odoo.define('croa_branding.UserMenu', function (require) {
    "use strict";

    require('croa_branding.base');
    var session = require('web.session');
    var core = require('web.core');
    var _t = core._t;


    var UserMenu = require('web.UserMenu');
    UserMenu.include({
        on_menu_documentation: function () {
            window.open('https://www.croa.pt/documentation/user', '_blank');
        },
        on_menu_support: function () {
            window.open('https://www.croa.pt/suport', '_blank');
        },

    });
});