# -*- coding: utf-8 -*-
# Copyright 2015-2016 Lorenzo Battistini - Agile Business Group
# Copyright 2015 Antiun Ingeniería S.L. <http://antiun.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import http


class CroaNotice(http.Controller):
    @http.route(
        "/croa_notice/ok", auth="public", website=True, type='json',
        methods=['POST'])
    def accept_notice(self):
        """Stop spamming with notice banner."""
        http.request.session["accepted_notice"] = True
        http.request.env['ir.ui.view'].search([
            ('type', '=', 'qweb')
            ]).clear_caches()
        return {'result': 'ok'}
