# -*- coding: utf-8 -*-
# Copyright 2015-2016 Lorenzo Battistini - Agile Business Group
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Croa Notice',
    'summary': 'Show croa notice',
    'version': '10.0.1.0.0',
    'category': 'Website',
    'author': "AyA - Soluctions",
    'website': 'http://www.aya.pt',
    'license': 'AGPL-3',
    'depends': ['web_responsive'],
    'data': [
        'templates/website.xml',
    ],
    'installable': True,
    'auto_install': False,
}
