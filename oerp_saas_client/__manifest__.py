{
    'name': "Communities SAAS Client",
    'version': "1.0.3",
    'author': "Communities",
    'category': "Tools",
    'support': "jjnf@communities.pt",
    'summary':'Interacts with OERP SAAS Server',
    'license':'LGPL-3',
    'depends': ['web'],
    'data': [
        'data/res.users.csv',
        'data/ir.rule.xml',
        'data/ir.config_parameter.xml',
        'views/ir_actions_todo.xml',
        #'views/res_users_views.xml',
        'views/oerp_saas_client_templates.xml',
    ],
    'demo': [],
    'images':[
        'static/description/1.jpg',
    ],
    'installable': True,
}