# -*- coding: utf-8 -*-
{
    'name': "Centro de Recolha Oficial de Animais - Adopção",

    'summary': """
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Communities - Comunicações, Lda",
    'website': "http://www.cro.pt",

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'cro_core'],

    # always loaded
    'data': [
        'views/animal_views.xml',
    ],
}
