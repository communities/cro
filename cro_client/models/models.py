# -*- coding: utf-8 -*-

import xmlrpclib
import string
import random

from odoo import api, fields, models, tools, _
from odoo.modules import get_module_resource
from odoo.osv.expression import get_unaccent_wrapper
from odoo.exceptions import UserError, ValidationError
from odoo.osv.orm import browse_record


class CroAnimais(models.Model):
	
    _inherit = "cro.animal"
    
    @api.one
    def send_to_portal(self):
    	#self.portal = 'published'
    	### dados ### 
    	url = 'http://localhost:8010'
    	db = 'www.croa.pt'
    	username ='admin'
    	password ='admin'
    	### /dados ###
 	
    	common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
    	common.version()
    	uid = common.authenticate(db, username, password, {})
     	models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))   	

      # variaveis
    	especie_id = self.especie_id.name
    	raca_id = self.raca_id.name
    	cor_id = self.cor_id.name
    	
    	 #dados a cria
    	values = {
    	'image': self.image,
    	'name': self.name,
    	'sexo': self.sexo,
    	'data_nasc': self.data_nasc,
    	'peso': self.peso,
    	'puni': self.puni,
    	'chip': self.chip,
    	'esterilizado': self.esterilizado,
    	'b_sanitario': self.b_sanitario,
    	'comportamento': self.comportamento,
    	'notas': self.notas,
    	'especie_id': especie_id,
    	'raca_id': raca_id,
    	'cor_id': cor_id,
    	'code': self.code}
    	
    	#cria registo
    	models.execute_kw(db, uid, password, 'cro.animal', 'create', [values])

############################################################################################
    #@api.one
    def update_in_portal(self):
    	### dados ### 
    	url = 'http://localhost:8010'
    	db = 'www.croa.pt'
    	username ='admin'
    	password ='admin'
    	### /dados ###
  	
    	common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
    	common.version()
    	uid = common.authenticate(db, username, password, {})
     	models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))   	

      # variaveis
    	especie_id = self.especie_id.name
    	raca_id = self.raca_id.name
    	cor_id = self.cor_id.name
    	
    	#dados a alterar
    	values = {
    	'image': self.image,
    	'name': self.name,
    	'sexo': self.sexo,
    	'data_nasc': self.data_nasc,
    	'peso': self.peso,
    	'puni': self.puni,
    	'chip': self.chip,
    	'esterilizado': self.esterilizado,
    	'b_sanitario': self.b_sanitario,
    	'comportamento': self.comportamento,
    	'notas': self.notas,
    	'especie_id': especie_id,
    	'raca_id': raca_id,
    	'cor_id': cor_id,
    	'code': self.code}
    	
 #   	if  values['code']  == self.code:
 #         print(self.code)
 #         models.execute_kw(db, uid, password, 'cro.animal', 'write', [values])
    	
  	
    	#pesquisa registo
    	args = [[['code', '=', self.code]]]
    	ids = models.execute_kw(db, uid, password, 'cro.animal', 'search', args)

    	print('###################################################################')    	
    	print(ids)

    	#update registo
    	#models.execute_kw(db, uid, password, 'cro.animal', 'write', [[ids],  {'data_nasc': self.data_nasc}])
    	#models.execute_kw(db, uid, password, 'cro.animal', 'write', args)
    	for id in ids:
    	    models.execute_kw(db, uid, password, 'cro.animal', 'write', [[id], {
    	    'image': self.image,
    	    'name': self.name,
    	    'sexo': self.sexo,
    	    'data_nasc': self.data_nasc,
    	    'peso': self.peso,
    	    'puni': self.puni,
    	    'chip': self.chip,
    	    'esterilizado': self.esterilizado,
    	    'b_sanitario': self.b_sanitario,
    	    'comportamento': self.comportamento,
    	    'notas': self.notas,
    	    'especie_id': especie_id,
    	    'raca_id': raca_id,
    	    'cor_id': cor_id,
    	    }])


############################################################################################
    @api.one
    def unlink_from_portal(self):
    	### dados ### 
    	url = 'http://localhost:8010'
    	db = 'www.croa.pt'
    	username ='admin'
    	password ='admin'
    	### /dados ###
 	
    	common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
    	common.version()
    	uid = common.authenticate(db, username, password, {})
     	models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))   	
    	
    	#pesquisa registo
    	args = [[['code', '=', self.code]]]
    	ids = models.execute_kw(db, uid, password, 'cro.animal', 'search', args)

    	print('###################################################################')    	
    	print(ids)
	
    	#Apaga registo (só para testar) funciona
    	models.execute_kw(db, uid, password, 'cro.animal', 'unlink', ids)

###### Fim nova proposta 

