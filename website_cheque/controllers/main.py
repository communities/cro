# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http
from odoo.http import request

class WebsiteCheque(http.Controller):

    @http.route('/page/camvs', type='http', auth="public", website=True)
    def camvs(self, **post):

        camvs = request.env['res.partner'].sudo().search([])
        return request.render("website_cheque.camvs", {'camvs': camvs})

    @http.route('/page/croas', type='http', auth="public", website=True)
    def croas(self, **post):

        croas = request.env['res.partner'].sudo().search([])
        return request.render("website_cheque.croas", {'croas': croas})