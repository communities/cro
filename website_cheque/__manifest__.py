# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name':'Website Cheque',
    'category': 'Website',
    'website': 'https://www.chequeveterinario.pt',
    'summary': 'Chequeveterinario',
    'version':'1.0',
    'description': """
    Website Chequeveterinario
==========================
        """,
    'author':'Communities - Comunicações, Lda',
    'data': [
        #'security/ir.model.access.csv',
        'views/assets.xml',
        'views/cheque_template.xml',
        #'views/homepage_template.xml',
        #'views/contact_template.xml',
        #'views/404_template.xml',
        'views/login_template.xml',
        'views/pages_template.xml',
        #'views/aboutus_template.xml',
    ],
    'depends': ['website'],
    'installable': True,
    'auto_install': False,
    'application': True,
    'live_test_url': 'http://www.chequeveterinario.pt',
}
