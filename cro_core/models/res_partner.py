# -*- coding: utf-8 -*-

from odoo import api, fields, models

class ResPartnerCroa(models.Model):
    _inherit = "res.partner"

    @api.one
    def _get_current_user(self):
        self.current_user = (self.env.user.company_id.id == self.company_id.id)

    current_user = fields.Boolean('is current user ?', compute='_get_current_user')
    iban = fields.Char(string='IBAN')
    cc_number = fields.Char(
        'BI/CC', size=16, help="Número do BI/CC")
    cc_archive = fields.Char(
        'Arquivo CC', size=32)
    emission_date = fields.Date(
        'Data de Emissão', help="Data de Emissão BI/CC")
    validity_date = fields.Date(
        'Data de Validade', help="Data de Validade BI/CC")
    animais_ids = fields.One2many('cro.animal', 'dono_id', string='Animais', readonly=True)
    adopcao_ids = fields.One2many('cro.processo.out', 'adoptante_id', string='Processos de Adopção', readonly=True)