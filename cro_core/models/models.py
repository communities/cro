# -*- coding: utf-8 -*-


import base64
import datetime
import hashlib
import pytz
import threading
import urllib2
import urlparse

from email.utils import formataddr
from lxml import etree

from odoo import api, fields, models, tools, _
from odoo.modules import get_module_resource
from odoo.osv.expression import get_unaccent_wrapper
from odoo.exceptions import UserError, ValidationError
from odoo.osv.orm import browse_record
from dateutil.relativedelta import relativedelta

@api.model
def especie_name_search(self, name='', args=None, operator='ilike', limit=100):
    if args is None:
        args = []

    records = self.browse()
    if len(name) == 2:
        records = self.search([('code', 'ilike', name)] + args, limit=limit)

    search_domain = [('name', operator, name)]
    if records:
        search_domain.append(('id', 'not in', records.ids))
    records += self.search(search_domain + args, limit=limit)

    # the field 'display_name' calls name_get() to get its value
    return [(record.id, record.display_name) for record in records]

   
class CroAnimais(models.Model):
    _description = 'Animais'
    _name = "cro.animal"
    _inherit = ['mail.thread']

    name = fields.Char(size=25, index=True)
    code = fields.Char(
        'Code', default=lambda self: self.env['ir.sequence'].next_by_code('processo.cro.animal.serial'),
        required=True, readonly=True, help="Unique Process/Serial Number")
    dono_id = fields.Many2one('res.partner', string='Dono', index=True)
    especie_id = fields.Many2one('cro.animal.especie', required=True, string='Espécie')
    raca_id = fields.Many2one('cro.animal.raca', required=True, string='Raça', index=True, domain="[('especie_id','=', especie_id)]")
    cruzado = fields.Selection([('yes',' Sim'), ('no',' Não')], default="no")
    cor_id = fields.Many2one('cro.animal.cor', string='Cor')
    box_id = fields.Many2one('cro.animal.box', string='Box de Acolhimento')
    sexo = fields.Selection([('macho','Macho'), ('femea','Fêmea'), ('outro','Não Definido')], default="outro", string="Sexo")    
    idade = fields.Float(string='Idade')
    iuni = fields.Selection([('mes',' Mes(es)'), ('ano',' Ano(s)')], default="ano")
    
    data_nasc = fields.Date('Data de Nacimento')
    c_idade = fields.Char(compute='_calculo_idade')
    
    peso = fields.Float(string='Peso')
    puni = fields.Selection([('grama',' Grama(s)'), ('quilograma',' Quilograma(s)')], default="quilograma")
    chip = fields.Char(string='Microchip')
    esterilizado = fields.Selection([('yes','Sim'), ('no','Não')], default="no", string="Esterilizado/Castrado")
    b_sanitario = fields.Selection([('yes','Sim'), ('no','Não')], default="no", string="Boletim Sanitário") 
    comportamento = fields.Selection([('docil','Dócil'), ('agressivo','Agressivo'), ('esquivo','Esquivo/Assilvestrado'), ('nd','Não Definido')], default="nd", string="Comportamento") 
    notas = fields.Text(string='Notas Internas')
    cr_ids = fields.One2many('cro.processo.cr', 'animal_id', string='Processos', readonly=True)
    adopcao_ids = fields.One2many('cro.processo.out', 'animal_id', string='Processos', readonly=True)
    tmv_ids = fields.One2many('cro.processo.tmv', 'animal_id', string='Processos', readonly=True)
    arquivo_boolean = fields.Boolean('Arquivado')
    company_id = fields.Many2one('res.company', string='CRO', required=True, default=lambda self: self.env.user.company_id)
    
    state = fields.Selection(
        [('resident', 'Residente'), ('adopted', 'Adoptado'),
         ('euthanized', 'Eutanasiado'), ('returned', 'Devolvido ao Detentor'), ('death', 'Óbito'), ('colonia', 'Colónia')],
        'State', readonly=True, index=True,
        default='resident', track_visibility='onchange')
        
    # image: all image fields are base64 encoded and PIL-supported
    image = fields.Binary("Image", attachment=True,
        help="This field holds the image used as avatar for this contact, limited to 1024x1024px",)
    image_medium = fields.Binary("Medium-sized image", attachment=True,
        help="Medium-sized image of this contact. It is automatically "\
             "resized as a 128x128px image, with aspect ratio preserved. "\
             "Use this field in form views or some kanban views.")
    image_small = fields.Binary("Small-sized image", attachment=True,
        help="Small-sized image of this contact. It is automatically "\
             "resized as a 64x64px image, with aspect ratio preserved. "\
             "Use this field anywhere a small image is required.")

    type = fields.Selection(
        [('abandonado', 'Abandonado'),
         ('desaparecido', 'Desaparecido'),
         ('outro', 'Outro')], string='Situação do Animal',
        default='outro')

    _sql_constraints = [
        ('chip_ref_uniq', 'unique (chip)', 'O Numero de identificação deve ser unico!'),
    ]

    @api.onchange('especie_id')
    def onchange_especie(self):
        self.raca_id = False

    @api.multi
    @api.depends('data_nasc')
    def _calculo_idade(self):
        for record in self:
            idade = relativedelta(fields.Date.from_string(fields.Date.today()), fields.Date.from_string(record.data_nasc))
            if record.data_nasc:# and idade.months <= 11 and not idade.years:
                record.c_idade = str(idade.years) + (' Ano(s) e ') + str(idade.months) + (' Mes(es)')
            else:
                record.c_idade = 0

    @api.model
    def create(self, vals):
        if not vals.get('image'):
            vals['image'] = self._get_default_image(vals.get('type'), vals.get('especie_id'), vals.get('dono_id'))
        tools.image_resize_images(vals)
        return super(CroAnimais, self).create(vals)

    @api.multi
    def write(self, vals):
        tools.image_resize_images(vals)
        return super(CroAnimais, self).write(vals)

    @api.multi
    def copy(self):
        raise ValidationError(_("Não é passível duplicar o registo"))

    @api.multi
    def unlink(self):
        raise ValidationError(_("Não é possível eliminar este registo"))
        
    @api.model
    def _get_default_image(self, dono_id, especie_id, raca_id):
        if getattr(threading.currentThread(), 'testing', False) or self._context.get('install_mode'):
            return False

        colorize, img_path, image = False, False, False

        if not image:
            img_path = get_module_resource('cro_core', 'static/src/img', 'dog.png')
        elif not image and dono_id:
            img_path = get_module_resource('cro_core', 'static/src/img', 'dog_1.png')
            colorize = True

        if img_path:
            with open(img_path, 'rb') as f:
                image = f.read()
        if image and colorize:
            image = tools.image_colorize(image)

        return tools.image_resize_image_big(image.encode('base64'))
                                    
class CroAnimaisEspecie(models.Model):
    _description = "Especie"
    _name = "cro.animal.especie"
    
    name = fields.Char(size=25, string="Espécie")
    descr = fields.Char(size=200, string="Descrição")

    
class CroAnimaisRaca(models.Model):
    _description = "Raca"
    _name = "cro.animal.raca"
    
    name = fields.Char(size=25, string="Raça")
    especie_id = fields.Many2one('cro.animal.especie', string='Espécie', required=True)
    descr = fields.Char(size=200, string="Descrição")

    name_search = especie_name_search

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if self.env.context.get('get_parent_especie', False):
            lst = []
            lst.append(self.env.context.get('especie_id'))
            especies = self.env['cro.animal.raca'].search([('especie_id', 'in', lst)])
            return especies.name_get()
        return super(CroAnimaisRaca, self).name_search(
            name, args, operator=operator, limit=limit)

    
class CroAnimaisCor(models.Model):
    _description = "Cor"
    _name = "cro.animal.cor"
    
    name = fields.Char(size=25, string="Cor")

    
class CroAnimaisBox(models.Model):
    _description = "Box"
    _name = "cro.animal.box"

    def _compute_animal_count(self):
        for box in self:
            box.animal_count = len(box.animal_ids)
    
    name = fields.Char(size=25, string="Identificação")
    especie_id = fields.Many2one('cro.animal.especie', string='Espécie')
    capacity = fields.Integer(string='Lotação')
    notas = fields.Text(string='Observações')
    color = fields.Integer(string='Color Index')
    animal_count = fields.Integer(compute='_compute_animal_count', string="Animais")
    label_registo = fields.Char(string='Use Records as', default='Animais', help="Gives label to Records on Box's kanban view.")
    animal_ids = fields.One2many('cro.animal', 'box_id', string='Animais', readonly=True, copy=False)
    company_id = fields.Many2one('res.company', string='CRO', required=True, default=lambda self: self.env.user.company_id)

    @api.multi
    def unlink(self):
        if self.animal_ids:
            raise ValidationError(_("Não é possível eliminar este alojamento"))
        return super(CroAnimaisBox, self).unlink()
    
class CroAnimaisProcessosIn(models.Model):
    _description = "Processo de Recolha ou Entrega"
    _name = "cro.processo.cr"
    _inherit = ['mail.thread']
   
    name = fields.Char(string='Processo', required=True, copy=False, readonly=True, index=True, default=lambda self: _('Rascunho'))    
    motivo = fields.Selection([('entregadetentor','Entrega por Detentor'), ('entreganaodetentor','Entrega por Não Detentor'), ('entregaautoridade','Entrega por Autoridade Policial'), ('entregaentidaderodoviaria','Entrega por Entidade Rodoviária'),  ('recolhaerrante','Recolha de Animal Errante'), ('recolhaparticular','Recolha de Animal a Particular'),  ('sequestro','Sequestro Sanitário'),  ('ced','Programa CED - Felinos')], required=True, string="Tipo de Entrada")
    local = fields.Char(string='Local de Ocorrência', help="Outro local de referência interna")
    county_id = fields.Many2one('res.county', string='Concelho de Ocorrência', required=True)
    local_id = fields.Many2one('res.county.local', domain="[('county_id','=', county_id)]", string='Freguesia/Local de Ocorrência', required=True)
    data = fields.Date('Data de Ocorrência', required=True)
    notas = fields.Text(string='Observações')
    animal_id = fields.Many2one('cro.animal', string='Animal', required=True,  domain="[('state', '!=', 'resident'), ('state', '!=', 'euthanized'), ('state', '!=', 'death')]")
    sequestro = fields.Selection([('agressaopessoa','Agressão a Pessoas'), ('agressaoanimal','Agressão a Outros Animais'), ('raiva','Suspeita de Raiva')], string="Motivo do Sequestro")
    condicao = fields.Selection([('saudavel','Aparentemente Saudável'), ('doente','Aparentemente Doente'), ('feridol','Ferimentos Ligeiros'), ('feridog','Ferimentos Graves') ], string="Condição Corporal")
    company_id = fields.Many2one('res.company', string='CRO', required=True, default=lambda self: self.env.user.company_id)
    box_id = fields.Many2one('cro.animal.box', string='Box de Acolhimento')
    
    state = fields.Selection(
        [('draft', 'Rascunho'), ('confirm', 'Confirmado'),
         ('cancel', 'Cancelado'), ('done', 'Impresso')],
        'State', readonly=True, index=True,
        default='draft', track_visibility='onchange')

#    @api.onchange('box_@id')
#    def _onchange_box_id(self):
#        if not self.box_id:
#            return
#        box = self.box_id
#        for animal in self.animal_id:
#            animal.box_id = box.id
        #self.animal_id.box_id = box.id

    @api.onchange('county_id')
    def onchange_county(self):
        self.local_id = False

    @api.one
    def confirm_in_progress(self):
        self.state = 'confirm'
        self.animal_id.state = 'resident'
        self.animal_id.arquivo_boolean = False
        self.animal_id.box_id = self.box_id
        self.animal_id.dono_id = self.company_id.partner_id
        if self.name == _('Rascunho'):
            self.name = self.env['ir.sequence'].next_by_code('processo.cro.serial') or _('Rascunho')          

    @api.one
    def confirm_to_draft(self):
        self.state = 'draft'

    @api.one
    def confirm_cancel(self):
        self.state = 'cancel'
        
    @api.multi
    def print_process(self):
        self.filtered(lambda s: s.state == 'confirm').write({'state': 'done'})
        return self.env['report'].get_action(self, 'cro_core.report_process_in')

    @api.multi
    def copy(self):
        raise ValidationError(_("Não é possível duplicar este registo"))

    @api.multi
    def unlink(self):
        if self.name != _('Rascunho'):
            raise ValidationError(_("Não é possível eliminar um processo confirmado"))
        return super(CroAnimaisProcessosIn, self).unlink()
        
    @api.constrains('motivo')
    @api.one
    def _check_ced_program(self):
        if self.motivo == 'ced' and self.animal_id.especie_id.name != 'Felina':
            raise ValidationError('O programa CED é exclusivo para felinos')

class CroAnimaisProcessosOut(models.Model):
    _description = "Processo de Adopcao e Outros"
    _name = "cro.processo.out"
    _inherit = ['mail.thread']
        
    name = fields.Char(string='Processo', required=True, copy=False, readonly=True, index=True, default=lambda self: _('Rascunho'))
    motivo = fields.Selection([('adopcao','Adopção'), ('devolucao','Devolução ao Detentor'), ('morte','Morte Natural'), ('eutanasia','Eutanásia'), ('entregaassociacao','Entrega a Associação de Proteção Animal'), ('colonia','Devolução à Colónia - Felinos') ], required=True, string="Tipo de Saida")
    local = fields.Char(string='Local de Ocorrência', help="Outro local de referência interna")
    county_id = fields.Many2one('res.county', string='Concelho de Ocorrência')
    local_id = fields.Many2one('res.county.local', domain="[('county_id','=', county_id)]", string='Freguesia/Local de Ocorrência')
    data = fields.Date('Data de Ocorrência', required=True)
    notas = fields.Text(string='Observações')
    adoptante_id = fields.Many2one('res.partner', string='Adoptante')
    animal_id = fields.Many2one('cro.animal', string='Animal', required=True, domain="[('state', '=', 'resident')]")
    eutanasia = fields.Selection([('eutanasia_a','Eutanásia por Agressividade'), ('eutanasia_z','Eutanásia por Doença Zoonótica'), ('eutanasia_c','Eutanásia por Doença Crónica'), ('eutanasia_f','Eutanásia por Ferimentos Graves')], string="Motivo da Eutanásia")
    company_id = fields.Many2one('res.company', string='CRO', required=True, default=lambda self: self.env.user.company_id)
    state = fields.Selection(
        [('draft', 'Rascunho'), ('confirm', 'Confirmado'),
         ('cancel', 'Cancelado'), ('done', 'Impresso')],
        'State', readonly=True, index=True,
        default='draft', track_visibility='onchange')

    @api.onchange('county_id')
    def onchange_county(self):
        self.local_id = False
        
#    @api.one
    @api.multi
    def confirm_in_progress(self):
        self.state = 'confirm'
        if self.name == _('Rascunho'):
            self.name = self.env['ir.sequence'].next_by_code('processo.cro.adopcao.serial') or _('Rascunho')
        if self.motivo == 'adopcao':
            self.animal_id.state = 'adopted'
            self.animal_id.arquivo_boolean = True
            self.animal_id.box_id = None
            self.animal_id.dono_id = self.adoptante_id
            
        if self.motivo == 'devolucao':
            self.animal_id.state = 'returned'
            self.animal_id.arquivo_boolean = True
            self.animal_id.box_id = None
            
        if self.motivo == 'morte':
            self.animal_id.state = 'death'
            self.animal_id.arquivo_boolean = True
            self.animal_id.box_id = None
            
        if self.motivo == 'eutanasia':
            self.animal_id.state = 'euthanized'
            self.animal_id.arquivo_boolean = True
            self.animal_id.box_id = None
            
        if self.motivo == 'colonia':
            self.animal_id.state = 'colonia'
            self.animal_id.arquivo_boolean = True
            self.animal_id.box_id = None

    @api.one
    def confirm_to_draft(self):
        self.state = 'draft'

    @api.one
    def confirm_cancel(self):
        self.state = 'cancel'
        self.animal_id.state = 'resident'
        self.animal_id.arquivo_boolean = False
        
    @api.multi
    def print_process(self):
        self.filtered(lambda s: s.state == 'confirm').write({'state': 'done'})
        return self.env['report'].get_action(self, 'cro_core.report_process_out')

    @api.multi
    def copy(self):
        raise ValidationError(_("Não é possível duplicar este registo"))

    @api.multi
    def unlink(self):
        if self.name != _('Rascunho'):
            raise ValidationError(_("Não é possível eliminar um processo confirmado"))
        return super(CroAnimaisProcessosOut, self).unlink()      
        
    @api.constrains('motivo')
    @api.one
    def _check_ced_program(self):
        if self.motivo == 'colonia' and self.animal_id.especie_id.name != 'Felina':
            raise ValidationError('O programa CED é exclusivo para felinos')


############################## Tratamentos
 
class CroAnimaisProcessosTmv(models.Model):
    _description = "Tratamento Médico Veterinário"
    _name = "cro.processo.tmv"
    _inherit = ['mail.thread']
    
    
    name = fields.Char(string='Processo', required=True, copy=False, readonly=True, index=True, default=lambda self: _('Rascunho'))   
    animal_id = fields.Many2one('cro.animal', string='Animal', required=True, domain="[('state', '=', 'resident')]")        
    checkin = fields.Datetime('Início de Tratamento', required=True)  
    checkout = fields.Datetime('Fim do Tratamento')  
    camv = fields.Many2one('res.company', string='CAMV', domain="[('partner_type', '=', 'camv'), ('active', '=', True)]")
    motivo = fields.Selection([('interno','Tratamento Interno'), ('externo','Tratamento Externo')], required="True", string="Motivo")
    tipo = fields.Selection([('01','Cirurgia'), ('02','Controlo'), ('03','Desparasitação'), ('04','Especialidade'), ('05','Exames Complementares'), ('06','Vacinação'), ('07','Outros Procedimentos')], string="Tratamentos") 
    notas = fields.Text(string='Observações')
    company_id = fields.Many2one('res.company', string='CRO', required=True, default=lambda self: self.env.user.company_id)
    state = fields.Selection(
        [('draft', 'Rascunho'), ('confirm', 'Em Tratamento'),
         ('done', 'Tratamento Concluído'), ('cancel', 'Cancelado')],
        'State', readonly=True, index=True,
        default='draft', track_visibility='onchange')

    @api.one
#    @api.multi
    def confirm_in_progress(self):
        self.state = 'confirm'
        if self.name == _('Rascunho'):
            self.name = self.env['ir.sequence'].next_by_code('processo.cro.processo.tmv.serial') or _('Rascunho')

    @api.constrains('checkout')
    @api.one
    def confirm_conclude(self):
        if not self.checkout:
            raise ValidationError('Para concluir o processo deve indicar uma data e hora de conclusão') 
        self.state = 'done'    

    @api.one
    def confirm_to_draft(self):
        self.state = 'draft'

    @api.one
    def confirm_cancel(self):
        self.state = 'cancel'
        
    @api.multi
    def print_process(self):
        #self.filtered(lambda s: s.state == 'confirm').write({'state': 'done'})
        return self.env['report'].get_action(self, 'cro_core.report_process_tmv')

    @api.multi
    def copy(self):
        raise ValidationError(_("Não é possível duplicar este registo"))

    @api.multi
    def unlink(self):
        if self.name != _('Rascunho'):
            raise ValidationError(_("Não é possível eliminar um processo confirmado"))
        return super(CroAnimaisProcessosTmv, self).unlink()   
                
##############################
class CroCadaver(models.Model):
    _description = "Cadáveres"
    _name = "cro.cadaver"
    
#    name = fields.Char(
#        'Processo', default=lambda self: self.env['ir.sequence'].next_by_code('processo.cro.cadaver.serial'),
#        required=True, readonly=True, help="Unique Process/Serial Number")
    
    name = fields.Char(string='Processo', required=True, copy=False, readonly=True, index=True, default=lambda self: _('Rascunho'))
    motivo = fields.Selection([('entregadetentor','Entrega por Detentor'), ('entreganaodetentor','Entrega por Não Detentor'), ('entregaautoridade','Entrega por Autoridade Policial'), ('entregaentidaderodoviaria','Entrega por Entidade Rodoviária'), ('recolhacadaver','Recolha de Cadáver na Via Pública'), ('recolhaparticular','Recolha de Cadáver ao Detentor')], required="True", string="Tipo de Entrega/Recolha")        
    data = fields.Date('Data de Ocorrência', required=True)
    notas = fields.Text(string='Observações')
    num_animais = fields.Char(string='Numero de Animais')
    peso = fields.Float(string='Peso')
    puni = fields.Selection([('grama',' Grama(s)'), ('quilograma',' Quilograma(s)')], default="quilograma")   
    
    animal_id = fields.Many2one('cro.animal', string='Animal', domain="[('state', '!=', 'resident')]")
    partner_id = fields.Many2one('res.partner', string='Entregador')
    company_id = fields.Many2one('res.company', string='CRO', required=True, default=lambda self: self.env.user.company_id)
    
    state = fields.Selection(
        [('draft', 'Rascunho'), ('confirm', 'Confirmado'),
         ('cancel', 'Cancelado'), ('done', 'Impresso')],
        'State', readonly=True, index=True,
        default='draft', track_visibility='onchange')
        
    @api.multi
    def confirm_in_progress(self):
        self.state = 'confirm'
        if self.name == _('Rascunho'):
            self.name = self.env['ir.sequence'].next_by_code('processo.cro.cadaver.serial') or _('Rascunho')
        if self.animal_id:
            self.animal_id.state = 'death'
            self.animal_id.arquivo_boolean = True

    @api.one
    def confirm_to_draft(self):
        self.state = 'draft'

    @api.one
    def confirm_cancel(self):
        self.state = 'cancel'
        
    @api.multi
    def print_process(self):
        self.filtered(lambda s: s.state == 'confirm').write({'state': 'done'})
        return self.env['report'].get_action(self, 'cro_core.report_cadaver')

    @api.multi
    def copy(self):
        raise ValidationError(_("Não é possível duplicar este registo"))

    @api.multi
    def unlink(self):
        if self.name != _('Rascunho'):
            raise ValidationError(_("Não é possível eliminar um processo confirmado"))
        return super(CroCadaver, self).unlink()

############################## EVE
 

class CroAnimaisProcessosEve(models.Model):
    _description = "Entregas Voluntarias para Eutanasia"
    _name = "cro.processo.eve"
    
#    name = fields.Char(
#        'Processo', default=lambda self: self.env['ir.sequence'].next_by_code('processo.cro.processo.eve.serial'),
#        required=True, readonly=True, help="Unique Process/Serial Number")
        
    name = fields.Char(string='Processo', required=True, copy=False, readonly=True, index=True, default=lambda self: _('Rascunho'))        
        
    motivo = fields.Selection([('doenca','Doença Grave ou Terminal'), ('agressividade','Comportamento Agressivo')], required="True", string="Motivo da Eutanásia")        
    data = fields.Date('Data de Ocorrência')
    notas = fields.Text(string='Relatório Médico')
    #dono_id = fields.Many2one('res.partner', string='Dono')
    animal_id = fields.Many2one('cro.animal', string='Animal', domain="[('state', '=', 'resident')]")
    company_id = fields.Many2one('res.company', string='CRO', required=True, default=lambda self: self.env.user.company_id)
    state = fields.Selection(
        [('draft', 'Rascunho'), ('confirm', 'Confirmado'),
         ('cancel', 'Cancelado'), ('done', 'Impresso')],
        'State', readonly=True, index=True,
        default='draft', track_visibility='onchange')

    @api.one
#    @api.multi
    def confirm_in_progress(self):
        self.state = 'confirm'
        if self.name == _('Rascunho'):
            self.name = self.env['ir.sequence'].next_by_code('processo.cro.processo.eve.serial') or _('Rascunho')
        if self.animal_id:
            self.animal_id.state = 'death'
            self.animal_id.arquivo_boolean = True

    @api.one
    def confirm_to_draft(self):
        self.state = 'draft'

    @api.one
    def confirm_cancel(self):
        self.state = 'cancel'
        
    @api.multi
    def print_process(self):
        self.filtered(lambda s: s.state == 'confirm').write({'state': 'done'})
        return self.env['report'].get_action(self, 'cro_core.report_process_eve')

    @api.multi
    def copy(self):
        raise ValidationError(_("Não é possível duplicar este registo"))

    @api.multi
    def unlink(self):
        if self.name != _('Rascunho'):
            raise ValidationError(_("Não é possível eliminar um processo confirmado"))
        return super(CroAnimaisProcessosEve, self).unlink()   
             
##############################


