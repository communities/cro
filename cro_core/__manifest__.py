# -*- coding: utf-8 -*-
{
    'name': "Centro de Recolha Oficial de Animais - Core",

    'summary': """
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Communities - Comunicações, Lda",
    'website': "http://www.cro.pt",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'kanban_draggable'],

    # always loaded
    'data': [
        'views/animal_views.xml',
        'views/partner_views.xml',
        'views/box_views.xml',
        'views/process_views.xml',
        'views/process_mv_views.xml',
        'views/cro_sequence_data.xml',
        'views/templates.xml',
        #'views/config_views.xml',
        'views/cadavers_views.xml',
        'views/eve_views.xml',
        'views/mail_views.xml',
        'report/process_report.xml',
        'report/process_report_templates.xml',
        'security/cro_security.xml',
        'security/ir.model.access.csv',
        'data/cro.animal.especie.csv',
        'data/cro.animal.raca.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
